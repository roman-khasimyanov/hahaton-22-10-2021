import { createStitches } from '@stitches/react';

const styleConfig = createStitches({});

export { styleConfig };