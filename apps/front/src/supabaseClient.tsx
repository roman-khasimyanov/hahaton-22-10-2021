import { createClient } from "@pankod/refine-supabase";
import { notification, Button } from "antd";
import { definitions } from "interfaces/supabase";

export const supabaseClient = createClient(
  "https://ibhhhcvdzzirntcttmso.supabase.co",
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzNDkxMTE0NywiZXhwIjoxOTUwNDg3MTQ3fQ.ShPNfofbeCvCPR_WWiCcMd1ObCAEWWhDFi0So01cdck"
);

supabaseClient
  .from('*')
  .on('*', payload => {
    switch (payload.table) {
      case 'News': {
        notification.info({
          message: `Добавлена новая новость: ${(payload.new as definitions['News']).title}`,
          btn: (
            <Button
              type="link"
              onClick={() => {
                document.location = `/news/show/${(payload.new as definitions['News']).id}`;
              }}
            >
              Просмотреть
            </Button>
          ),
        });
        break;
      }
      case 'Event': {
        notification.info({
          message: `Добавлено новое событие: ${(payload.new as definitions['Event']).title}`,
          btn: (
            <Button
              type="link"
              onClick={() => {
                document.location = `/calendar`;
              }}
            >
              Просмотреть
            </Button>
          ),
        });
        break;
      }
      case 'Idea': {
        notification.info({
          message: `Добавлено новая идея: ${(payload.new as definitions['Idea']).title}`,
          btn: (
            <Button
              type="link"
              onClick={() => {
                document.location = `/idea/show/${(payload.new as definitions['Idea']).id}`;
              }}
            >
              Просмотреть
            </Button>
          ),
        });
        break;
      }
      case 'profile': {
        (async () => {
          const authUser = supabaseClient.auth.user();
          if ((payload.new as definitions['profile']).auth_user_id === authUser?.id) {
            const currency_span = document.getElementById('user_currency');
            console.log(currency_span);
            if (currency_span) {
              const currentCurrency = parseInt(currency_span.innerText);
              if (currentCurrency !== (payload.new as definitions['profile']).currency) {
                currency_span.innerText = (payload.new as definitions['profile']).currency.toString();
                notification.info({ message: 'Количество валюты было изменено' });
              }
            }
            const carma_span = document.getElementById('user_carma');
            if (carma_span) {
              const currentCarma = parseInt(carma_span.innerText);
              if (currentCarma !== (payload.new as definitions['profile']).carma) {
                carma_span.innerText = (payload.new as definitions['profile']).carma?.toString() ?? '0';
                notification.info({ message: 'Количество кармы было изменено' });
              }
            }
          }
        })();
        break;
      }
    }
  })
  .subscribe();