export type UserMetada = {
	avatar_url?: string;
	full_name?: string;
	provider_id?: string;
	birth_day?: string;
}