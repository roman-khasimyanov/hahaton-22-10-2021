import {
  List,
  TextField,
  TagField,
  DateField,
  Table,
  useTable,
} from "@pankod/refine";
import { useTranslation } from "react-i18next";

export const UsersList: React.FC = () => {
  const { t, i18n } = useTranslation();
  return (
    <List
      title={t('user.user')}
    >
      <Table rowKey="id">
        <Table.Column dataIndex="title" title="title" />
        <Table.Column
          dataIndex="status"
          title="status"
          render={(value) => <TagField value={value} />}
        />
        <Table.Column
          dataIndex="createdAt"
          title="createdAt"
          render={(value) => <DateField format="LLL" value={value} />}
        />
      </Table>
    </List>
  );
};