import { styleConfig } from "theme";

export const wrapper = styleConfig.css({
  background: 'url(bg.jpg)',
  backgroundSize: "cover",
  padding: '0px 8px',
  position: 'relative',
});

export const leftBlock = styleConfig.css({
  padding: 24,
  background: 'rgba(0, 0, 0, 0.9)',
  display: 'flex',
  paddingTop: '10%',
  flexDirection: 'column',
});

export const row = styleConfig.css({
  height: '100vh',
  justifyContent: 'center'
});

export const form = styleConfig.css({
  width: '100%',
});

export const video = styleConfig.css({
  objectFit: 'cover',
  width: '100%',
  height: '100%',
  position: 'absolute',
  top: 0,
  left: 0,
});