import { ArrowLeftOutlined, UploadOutlined } from "@ant-design/icons";
import {
	AntdLayout,
	Typography,
	useCreate,
	useLogin,
	useNavigation,
} from "@pankod/refine";
import { Button, Input, Form, Row, Col, DatePicker, Upload } from "antd";
import { definitions } from "interfaces/supabase";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { supabaseClient } from "supabaseClient";
import { form, leftBlock, row, video, wrapper } from "./index.styles";
import ruLocale from "antd/es/date-picker/locale/ru_RU";
import "dayjs/locale/ru";
import dayjs from "dayjs";

export const SignupPage: React.FC = () => {
	const { replace, goBack, push } = useNavigation();
	const { t } = useTranslation();
	const [formInst] = Form.useForm();
	return (
		<AntdLayout className={wrapper()}>
			<video
				playsInline
				autoPlay
				muted
				loop
				poster="bg.jpg"
				id="bgvid"
				className={video()}
			>
				<source src="bg.mp4" type="video/mp4" />
			</video>
			<Row gutter={16} className={row()}>
				<Col span={12} xs={0} sm={0} md={12} xl={12} xxl={12}></Col>
				<Col
					span={12}
					xs={24}
					sm={24}
					md={12}
					xl={12}
					xxl={12}
					className={leftBlock()}
				>
					<Typography.Title level={2} style={{ textAlign: "center" }}>
						Регистрация
					</Typography.Title>
					<Form
						form={formInst}
						onFinish={(values) => {
							supabaseClient.auth
								.signUp(
									{
										email: values.email,
										password: values.password,
									},
									{
										data: {
											full_name: values.full_name,
											avatar_url: values.avatar_url,
											birth_day: values.birth_day || new Date(),
											currency: 0,
											carma: 0,
										},
									}
								)
								.then((res) => {
									if (!res.error) {
										push("/");
									}
								});
						}}
						layout="vertical"
						className={form()}
						size="large"
					>
						<Form.Item name="email" label="Email">
							<Input type="email" placeholder="Введите email" />
						</Form.Item>
						<Form.Item name="password" label="Пароль">
							<Input type="password" placeholder="Введите пароль" />
						</Form.Item>
						<Form.Item name="full_name" label="Полное имя">
							<Input placeholder="Введите имя" />
						</Form.Item>
						<Form.Item name="birth_day" label="Дата рождения">
							<DatePicker
								placeholder="Выберите дату рождения"
								style={{ width: "100%" }}
								format={dayjs().format("DD.MM.YYYY")}
								locale={ruLocale}
							/>
						</Form.Item>
						<Form.Item name="avatar_url" label="Фото профиля">
							<Upload
								maxCount={1}
								beforeUpload={(file) => {
									supabaseClient.storage
										.from("all")
										.upload(Date.now() + file.name, file, {
											cacheControl: "3600",
											upsert: false,
										})
										.then((response) => {
											formInst.setFieldsValue({
												avatar_url: `https://ibhhhcvdzzirntcttmso.supabase.in/storage/v1/object/public/${response.data?.Key}`,
											});
										});
									return false;
								}}
							>
								<Button icon={<UploadOutlined />}>Загрузить файл</Button>
							</Upload>
						</Form.Item>
						<Form.Item>
							<Row gutter={16}>
								<Col span={12}>
									<Button htmlType="submit" type="primary" size="large" block>
										{t("pages.login.signup")}
									</Button>
								</Col>
								<Col span={12}>
									<Button
										onClick={() => goBack()}
										type="default"
										size="large"
										block
									>
										Отмена
									</Button>
								</Col>
							</Row>
						</Form.Item>
					</Form>
				</Col>
			</Row>
		</AntdLayout>
	);
};
