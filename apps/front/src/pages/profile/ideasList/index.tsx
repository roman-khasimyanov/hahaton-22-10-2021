import { useList, AntdList, useMany, Skeleton, List, Link, Divider } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Col, Row, Select } from "antd";
import { UserById } from "components/User/UserById";
import "./index.css";
import { IdeaLikes } from "pages/idea/IdeaLikes";
import { IdeaFavoriteButton } from "pages/idea/IdeaFavoriteButton";
import RichTextViewer from "components/RichTextViewer";
import { useEffect } from "react";
import dayjs from "dayjs";
import { useTranslation } from "react-i18next";

const { Option } = Select;

type UserIdeasListProps = {
	user: definitions["profile"];
};

export const UserIdeasList = ({ user }: UserIdeasListProps) => {
	const { data: favoriteIdeas, isLoading: isLoadingFavoriteIdeas, refetch: refetchFavorites } = useList<
		definitions["IdeaFavorite"]
	>({
		resource: "IdeaFavorite",
		config: {
			filters: [
				{
					field: "user_id",
					value: user.auth_user_id,
					operator: "eq",
				},
			],
		},
	});

	const { data: ideas, isLoading: isLoadingIdeas } = useList<
		definitions["Idea"]
	>({
		resource: "Idea",
		config: {
			pagination: { pageSize: 10000 },
			filters: [
				{
					field: "id",
					value: favoriteIdeas?.data?.map((val) => val.idea_id ?? "") ?? [],
					operator: 'in',
				}
			],
		}
	});

	const {
		data: ideaLikes,
		isLoading: isLoadingIdeaLikes,
		refetch: refetchLikes,
	} = useList<definitions["IdeaLike"]>({
		resource: "IdeaLike",
	});

	useEffect(() => {
		refetchFavorites?.();
		refetchLikes?.();
	}, [user, refetchFavorites, refetchLikes]);
	console.log(favoriteIdeas);

	if (user && favoriteIdeas && ideas && ideaLikes) {
		return (
			<List title="">
				<Col>
					<Row justify="start">
						<Select defaultValue="title">
							<Option value="title">По наименованию</Option>
							<Option value="rating">По популярности</Option>
						</Select>
					</Row>
					<Divider />
					<Row>
						<AntdList
							dataSource={ideas?.data}
							renderItem={(item) => {
								const likes = ideaLikes.data.filter(
									(like) => like.idea_id === item.id
								);
								return (
									<AntdList.Item className="list__item">
										<Col style={{ width: "100%" }}>
											<Row justify="space-between" align="middle">
												<Link
													className="list__title"
													style={{ color: "#E0529C" }}
													to={`/idea/show/${item.id}`}
												>
													{item.title}
												</Link>
												<div className="data__item">
													{dayjs(item.created_at).format("L")}
												</div>
											</Row>
											{item.created_by &&
												<UserById id={item.created_by} />
											}
											<div className="list__value">
												<RichTextViewer initialValue={item.text} />
											</div>

											<Row>
												<IdeaFavoriteButton
													ideaFavoriteId={
														favoriteIdeas?.data.find(
															(currentElement) =>
																item.id === currentElement.idea_id
														)?.id
													}
													ideaId={item.id}
													userId={user?.auth_user_id}
												/>
												<IdeaLikes
													userId={user?.auth_user_id}
													ideaId={item.id}
													ideaLikeId={
														likes.find(
															(currentElement) =>
																user.auth_user_id === currentElement.user_id
														)?.id
													}
													countLike={likes.length}
												/>
											</Row>
										</Col>
									</AntdList.Item>
								);
							}}
						/>
					</Row>
				</Col>
			</List>
		);
	} else {
		return <Skeleton />;
	};
};
