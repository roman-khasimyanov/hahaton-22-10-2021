import {
  useList,
  AntdList,
  Skeleton,
  useUpdate,
  Modal,
  Form,
  Input,
  useCreate,
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Button, Col, List, Row } from "antd";
import "./index.css";
import { useState } from "react";

export const NewsTopicList = () => {
  const [isVisibleUpdateModal, setIsVisibleUpdateModal] = useState(false);
  const [isVisibleCreateTopicModal, setIsVisibleCreateTopic] = useState(false);
  const [createForm] = Form.useForm();
  const { data: newsTopicData, isLoading } = useList<definitions["NewsTopic"]>({
    resource: "NewsTopic",
    config: {
      pagination: {
        pageSize: 10000,
      },
    },
  });
  const [form] = Form.useForm();
  const { mutate: create } = useCreate();
  const { mutate: updateTopic } = useUpdate();
  const onUpdateTopic = (item: definitions["NewsTopic"]) => {
    setIsVisibleUpdateModal(true);
    form.setFieldsValue({
      name: item.name,
      id: item.id,
    });
  };

  if (isLoading) {
    return <Skeleton />;
  }

  return (
    <List
      header={
        <div
          style={{
            display: "inline-flex",
            justifyContent: "space-between",
            flexDirection: "row",
            width: "100%",
          }}
        >
          <span style={{ display: "block" }}>Топики новостей</span>
          <Button
            onClick={() => {
              setIsVisibleCreateTopic(!isVisibleCreateTopicModal);
            }}
          >
            Добавить новый топик
          </Button>
        </div>
      }
    >
      <Modal
        visible={isVisibleCreateTopicModal}
        onCancel={() => setIsVisibleCreateTopic(!isVisibleCreateTopicModal)}
        onOk={() => {
          create({
            resource: "NewsTopic",
            values: { name: createForm.getFieldValue("name") },
          });
          setIsVisibleCreateTopic(!isVisibleCreateTopicModal);
        }}
      >
        <Form title="Создание топика" form={createForm}>
          <Form.Item name="name" label="Название" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
      <Modal
        visible={isVisibleUpdateModal}
        onCancel={() => setIsVisibleUpdateModal(!isVisibleUpdateModal)}
        onOk={() => {
          updateTopic({
            resource: "NewsTopic",
            id: form.getFieldValue("id"),
            values: { name: form.getFieldValue("name") },
          });
          setIsVisibleUpdateModal(!isVisibleUpdateModal);
        }}
      >
        <Form title="Редактирование топика" form={form} onFinish={() => {}}>
          <Form.Item name="name" label="Название" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
      <Col>
        <Row>
          <AntdList
            dataSource={newsTopicData?.data}
            renderItem={(item) => (
              <AntdList.Item className="list__item">
                <Row
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <Col>
                    <div>{item.name}</div>
                  </Col>
                  <Col>
                    <Button type="link" onClick={() => onUpdateTopic(item)}>
                      Изменить топик
                    </Button>
                  </Col>
                </Row>
              </AntdList.Item>
            )}
          />
        </Row>
      </Col>
    </List>
  );
};
