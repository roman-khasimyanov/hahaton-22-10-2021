import { List, Card, Form, Skeleton, useUpdate } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { useTranslation } from "react-i18next";
import { supabaseClient } from "supabaseClient";
import { EditOutlined, SaveOutlined } from "@ant-design/icons";
import ruLocale from "antd/es/date-picker/locale/ru_RU";
import { useUser } from "hooks/useUser";
import { DatePicker, Input, Upload, Collapse, Image } from "antd";
import moment from "moment";
import { ReactNode, useEffect, useState } from "react";
import { UserIdeasList } from "./ideasList";
import { UserNewsList } from "./newsList";
import { UsersList } from "./usersList";
import { Vacation } from "./vacation";
import { NewsTopicList } from "./newsTopicList";
import { Purchases } from "components/Purchases";
import { GoodsList } from "./goodsList";

export const ProfileList = () => {
  const { t } = useTranslation();
  const user = useUser();
  const [profileFormInstanse] = Form.useForm();

  const [profileEdit, setProfileEdit] = useState(false);
  const { mutate } = useUpdate<definitions["profile"]>();

  const [profileActions, setProfileActions] = useState<Array<ReactNode>>([
    <EditOutlined key="edit" onClick={() => setProfileEdit(!profileEdit)} />,
  ]);

  useEffect(() => {
    if (profileEdit) {
      setProfileActions([
        <EditOutlined
          key="edit"
          onClick={() => setProfileEdit(!profileEdit)}
        />,
        <SaveOutlined
          key="save"
          onClick={() => profileFormInstanse.submit()}
        />,
      ]);
    } else {
      setProfileActions([
        <EditOutlined
          key="edit"
          onClick={() => setProfileEdit(!profileEdit)}
        />,
      ]);
    }
  }, [profileEdit]);

  if (user) {
    return (
      <List title={t("profile.profile")}>
        <Card title="Настройки профиля">
          <div style={{ display: "flex" }}>
            <Card
              style={{ width: 240, height: "fit-content" }}
              cover={<Image alt={user.avatar_url} src={user.avatar_url} />}
              actions={[
                <Upload
                  maxCount={1}
                  showUploadList={false}
                  beforeUpload={(file) => {
                    supabaseClient.storage
                      .from("all")
                      .upload(Date.now() + file.name, file, {
                        cacheControl: "3600",
                        upsert: false,
                      })
                      .then((response) => {
                        mutate({
                          resource: "profile",
                          id: user.id,
                          values: {
                            avatar_url: `https://ibhhhcvdzzirntcttmso.supabase.in/storage/v1/object/public/${response.data?.Key}`,
                          },
                        });
                      });
                    return false;
                  }}
                >
                  <EditOutlined />
                </Upload>,
              ]}
              bodyStyle={{ display: "none" }}
            />
            <div style={{ marginLeft: 15, width: "100%" }}>
              <Card actions={profileActions} title="Данные пользователя">
                <Form
                  form={profileFormInstanse}
                  layout="vertical"
                  initialValues={{
                    email: user.email,
                    name: user.full_name,
                    birth_day: user.birthday
                      ? moment(user.birthday)
                      : undefined,
                  }}
                  onFinish={(values) => {
                    mutate({
                      resource: "profile",
                      id: user.id,
                      values: {
                        email: values.email,
                        full_name: values.name,
                        birthday: values.birth_day,
                      },
                    });
                    // await supabaseClient.auth.update({ email: values.email });
                    setProfileEdit(false);
                  }}
                >
                  <Form.Item name="email" label="Email">
                    <Input
                      disabled={true}
                      type="email"
                      placeholder="Введите email"
                    />
                  </Form.Item>
                  <Form.Item name="name" label="Имя">
                    <Input disabled={!profileEdit} placeholder="Введите имя" />
                  </Form.Item>
                  <Form.Item name="birth_day" label="Дата рождения">
                    <DatePicker
                      style={{ width: "100%" }}
                      disabled={!profileEdit}
                      placeholder="Выберите дату рождения"
                      format={"DD.MM.YYYY"}
                      locale={ruLocale}
                      inputReadOnly={true}
                    />
                  </Form.Item>
                </Form>
              </Card>
              {/* <Card
									title="Смена пароля"
									style={{ marginTop: 10 }}
									actions={[
										<SaveOutlined key="save" onClick={() => passwordChangeFormInstanse.submit()} />
									]}
								>
									<Form
										layout="vertical"
										form={passwordChangeFormInstanse}
										onFinish={async (values) => {
											await supabaseClient.auth.update({ password: values.password });
										}}
									>
										<Form.Item name="password" label="Новый пароль">
											<Input placeholder="Введите новый пароль" />
										</Form.Item>
									</Form>
								</Card> */}
            </div>
          </div>
        </Card>
        <Collapse accordion>
          <Collapse.Panel key={1} header="Мои покупки">
            <Purchases />
          </Collapse.Panel>
          <Collapse.Panel key={2} header="Избранные идеи">
            <UserIdeasList user={user} />
          </Collapse.Panel>
          <Collapse.Panel key={5} header="Избранные новости">
            <UserNewsList user={user} />
          </Collapse.Panel>
          <Collapse.Panel key={3} header="Отпуск">
            <Vacation />
          </Collapse.Panel>
          {user.is_admin && (
            <Collapse.Panel key={4} header="Топики новостей">
              <NewsTopicList />
            </Collapse.Panel>
          )}
          {user.is_admin && (
            <Collapse.Panel key={6} header="Список пользователей">
              <UsersList />
            </Collapse.Panel>
          )}
          {user.is_admin && (
            <Collapse.Panel key={7} header="Список товаров">
              <GoodsList />
            </Collapse.Panel>
          )}
        </Collapse>
      </List>
    );
  } else {
    return <Skeleton />;
  }
};
