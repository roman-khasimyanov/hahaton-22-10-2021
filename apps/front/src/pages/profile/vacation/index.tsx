import { Button, Col, DatePicker, Form, Row, Skeleton, Timeline } from 'antd';
import React, { useEffect } from 'react';
import dayjs from 'dayjs';
import { useCreate, useGetLocale, useList } from '@pankod/refine';
import { useUser } from 'hooks/useUser';
import { definitions } from 'interfaces/supabase';
import enLocale from "antd/es/date-picker/locale/en_US";
import ruLocale from "antd/es/date-picker/locale/ru_RU";

const { RangePicker } = DatePicker;

export const Vacation = () => {
  const locale = useGetLocale();
  const { mutate: createMutate } = useCreate();
  const user = useUser();
  const [formInstanse] = Form.useForm();
  const { data: vacations, refetch } = useList<
    definitions["Vacation"]
  >({
    resource: "Vacation",
    config: {
      filters: [
        {
          field: "user_id",
          value: user?.auth_user_id,
          operator: "eq",
        },
      ],
      sort: [{ field: 'from', order: 'asc' }]
    },
  });
  useEffect(() => {
    refetch?.();
  }, [user, refetch])
  if (user && vacations) {
    return (
      <>
        <Form
          form={formInstanse}
          layout="vertical"
          onFinish={(values) => {
            createMutate({
              resource: "Vacation",
              values: {
                from: values.date[0],
                to: values.date[1],
                user_id: user?.auth_user_id,
              },
            });
            formInstanse.resetFields();
          }}
        >
          <Row align="middle">
            <Form.Item name="date">
              <RangePicker locale={locale() === "en" ? enLocale : ruLocale} />
            </Form.Item>
            <Form.Item>
              <Button htmlType='submit' style={{ marginLeft: '24px' }}>Добавить</Button>
            </Form.Item>
          </Row>
        </Form>

        <Timeline mode="left">
          {vacations.data.map((vac) => {
            return (
              <Timeline.Item color="#e0529c">
                {dayjs(vac.from).format('L')} - {dayjs(vac.to).format('L')}
              </Timeline.Item>
            );
          })}
        </Timeline>
      </>
    );
  } else {
    return <Skeleton />;
  }
}