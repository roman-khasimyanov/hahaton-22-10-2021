import { Form, Radio, FormInstance, TextField } from "@pankod/refine"

type CarmaEditFieldProps = {
	currentValue: number;
	formInstance?:  FormInstance;
}

export const CarmaEditField = ({ currentValue, formInstance }: CarmaEditFieldProps) => {
	return (
		<div style={{ display: 'flex' }}>
			<Form.Item
				name="carma"
				style={{ margin: 0, width: '40px' }}
			>
				<TextField value={formInstance?.getFieldValue('carma')} />
			</Form.Item>
			<Radio.Group style={{ marginLeft: '5px' }} onChange={({ target }) => formInstance?.setFieldsValue({ carma: currentValue + target.value })}>
				<Radio.Button value={5}>+5</Radio.Button>
				<Radio.Button value={10}>+10</Radio.Button>
				<Radio.Button value={15}>+15</Radio.Button>
			</Radio.Group>
		</div>
	)
}