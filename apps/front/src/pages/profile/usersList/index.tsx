import {
	List,
	Form,
	Input,
	DeleteButton,
	EditButton,
	Table,
	TextField,
	Switch,
	SaveButton,
	useEditableTable,
	Button,
	useUpdate,
	Radio,
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { Space } from 'antd';
import { useEffect, useState } from "react";
import { CarmaEditField } from "./carmaEditField";

export const UsersList = () => {
	const {
		sorter,
		tableProps,
		formProps,
		isEditing,
		saveButtonProps,
		cancelButtonProps,
		editButtonProps,

	} = useEditableTable<definitions['profile']>({
		resource: 'profile',
		initialSorter: [{ field: 'full_name', order: 'asc' }],
	});

	const [currentCarma, setCurrentCarma] = useState(0);

	return (
		<List title="">
			<Form {...formProps} onFinish={(values) => {
				const currentCurrency = formProps.form?.getFieldValue('currency');
				const newCurrency = currentCurrency + Math.round((((values as definitions['profile']).carma || 0) - currentCarma) / 10);
				formProps.onFinish?.({ currency: newCurrency, ...values });
			}}>
				<Table {...tableProps} rowKey="full_name">
					<Table.Column<definitions['profile']>
						key="full_name"
						dataIndex="full_name"
						title="ФИО"
					/>
					<Table.Column<definitions['profile']>
						key="currency"
						dataIndex="currency"
						title="Валюта"
					/>
					<Table.Column<definitions['profile']>
						key="carma"
						dataIndex="carma"
						title="Карма"
						render={(value, record): React.ReactNode => {
							if (isEditing(record.id)) {
								setCurrentCarma(value);
								return (
									<CarmaEditField currentValue={value} formInstance={formProps.form} />
								);
							}
							return <TextField value={value} />;
						}}
					/>
					<Table.Column<definitions['profile']>
						key="is_admin"
						dataIndex="is_admin"
						title="Админ"
						render={(value, record): React.ReactNode => {
							if (isEditing(record.id)) {
								return (
									<Form.Item
										name="is_admin"
										style={{ margin: 0 }}
									>
										<Switch defaultChecked={value} />
									</Form.Item>
								);
							}
							return <Switch checked={value} />;
						}}
					/>
					<Table.Column<definitions['profile']>
						title="Actions"
						dataIndex="actions"
						key="actions"
						render={(_text, record) => {
							if (isEditing(record.id)) {
								return (
									<Space>
										<SaveButton
											{...saveButtonProps}
											size="small"
										/* onClick={() => {
											const { form } = { ...formProps };
											mutate({
												resource: 'profile', values: {
													currency: form?.getFieldValue('currency') || 0,
													carma: form?.getFieldValue('carma'),
												}, id: record.id
											})
										}} */
										/>
										<Button
											{...cancelButtonProps}
											size="small"
										>
											Отмена
										</Button>
									</Space>
								);
							}
							return (
								<Space>
									<EditButton
										{...editButtonProps(record.id)}
										size="small"
									/>
								</Space>
							);
						}}
					/>
				</Table>
			</Form>
		</List>
	);

};
