import {
	List,
  useList,
  Row,
  Col,
  useMany,
  Skeleton,
	Select,
	Divider,
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { NewsCard } from "pages/news/NewsCard";

type UserNewListProps = {
	user: definitions['profile'];
}

export const UserNewsList = ({ user }: UserNewListProps) => {
	const { data: favoriteNews, isLoading: isLoadingFavoriteNews } = useList<
		definitions["NewsFavorite"]
	>({
		resource: "NewsFavorite",
		config: {
			filters: [
				{
					field: "user_id",
					value: user.auth_user_id,
					operator: "eq",
				},
			],
		},
	});

	const { data: newsData, isLoading: isLoadingNews } = useList<
		definitions["News"]
	>({
		resource: "News",
		config: {
			pagination: { pageSize: 10000 },
			filters: [
				{
					field: "id",
					value: favoriteNews?.data?.map((val) => val.news_id ?? "") ?? [],
					operator: 'in',
				}
			],
		}
	});

  const { data: newsTopicData, isLoading: isLoadingNewsTopic } = useMany<
    definitions["NewsTopic"]
  >({
    resource: "NewsTopic",
    ids:
      newsData?.data
        .filter((news) => news.topic_id)
        .map((news) => news.topic_id!) ?? [],
  });

  if (isLoadingNews || isLoadingNewsTopic) {
    return <Skeleton />;
  }

	return (
		<List title="">
			<Row gutter={16}>
				<Row>
					<Select defaultValue="title">
						<Select.Option value="title">По наименованию</Select.Option>
						<Select.Option value="rating">По популярности</Select.Option>
					</Select>
				</Row>
				<Divider />
				{newsData?.data.map((news) => (
					<Col span={24}>
						<NewsCard
							key={news.id}
							news={news}
							topic={newsTopicData?.data.find(
								(topic) => topic.id === news.topic_id
							)}
						/>
						<Divider />
					</Col>
				))}
			</Row>
		</List>
	);

};
