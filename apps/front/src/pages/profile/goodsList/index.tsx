// import React from "react";
// import { useEditableTable, Table, useList } from "@pankod/refine";
// import { definitions } from "interfaces/supabase";

// const columns = [
//   {
//     title: "Изображение",
//     dataIndex: "poster",
//     render: (text: string, record: any) => (
//       <img src={text} style={{ width: 200, height: 200 }} alt={text} />
//     ),
//   },
//   {
//     title: "Название",
//     dataIndex: "name",
//   },
//   {
//     title: "Описание",
//     dataIndex: "description",
//   },
//   {
//     title: "Цена",
//     dataIndex: "price",
//   },
// ];

// export const GoodsList = () => {
//   const editableTableProps = useEditableTable();
//   const { data } = useList<definitions["Goods"]>({
//     resource: "Goods",
//     config: { pagination: { pageSize: 10000 } },
//   });

//   return (
//     <Table {...editableTableProps} dataSource={data?.data} columns={columns} />
//   );
// };
import {
  List,
  Form,
  Input,
  EditButton,
  Table,
  TextField,
  SaveButton,
  useEditableTable,
  Button,
  Modal,
  useCreate,
  useSelect,
  InputNumber,
  useList,
  Select,
} from "@pankod/refine";
import { ArrowLeftOutlined, UploadOutlined } from "@ant-design/icons";
import { definitions } from "interfaces/supabase";
import { Space, Upload } from "antd";
import { supabaseClient } from "supabaseClient";
import { useState } from "react";

export const GoodsList = () => {
  const {
    tableProps,
    formProps,
    isEditing,
    saveButtonProps,
    cancelButtonProps,
    editButtonProps,
  } = useEditableTable<definitions["Goods"]>({
    resource: "Goods",
  });
  const { data: goodsTopicData, isLoading } = useList<
    definitions["GoodsTopic"]
  >({
    resource: "GoodsTopic",
  });
  const [isVisibleCreateModal, setIsVisibleCreateModal] = useState(false);
  const [form] = Form.useForm();
  const { mutate } = useCreate<definitions["Goods"]>();
  const { selectProps } = useSelect<definitions["GoodsTopic"]>({
    resource: "GoodsTopic",
    optionLabel: "name",
    optionValue: "id",
  });

  const CreateGoodsModal = () => (
    <Modal
      title="Создание товара"
      visible={isVisibleCreateModal}
      onCancel={() => setIsVisibleCreateModal(!isVisibleCreateModal)}
      onOk={() => {
        mutate({
          resource: "Goods",
          values: {
            name: form.getFieldValue("name"),
            description: form.getFieldValue("description"),
            price: form.getFieldValue("price"),
            poster: form.getFieldValue("poster"),
            topic_id: form.getFieldValue("topicId"),
          },
        });
        setIsVisibleCreateModal(!isVisibleCreateModal);
      }}
    >
      <Form form={form}>
        <Form.Item name="name" label="Название">
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Описание товара">
          <Input />
        </Form.Item>
        <Form.Item name="price" label="Цена товара">
          <InputNumber />
        </Form.Item>
        <Form.Item name="poster" label="Постер товара">
          <Upload
            maxCount={1}
            beforeUpload={(file) => {
              supabaseClient.storage
                .from("all")
                .upload(Date.now() + file.name, file, {
                  cacheControl: "3600",
                  upsert: false,
                })
                .then((response) => {
                  form.setFieldsValue({
                    poster: `https://ibhhhcvdzzirntcttmso.supabase.in/storage/v1/object/public/${response.data?.Key}`,
                  });
                });
              return false;
            }}
          >
            <Button icon={<UploadOutlined />}>Загрузить файл</Button>
          </Upload>
        </Form.Item>
        <Form.Item name="topicId" label="Топик">
          <Select {...selectProps} />
        </Form.Item>
      </Form>
    </Modal>
  );

  return (
    <List
      title="Доступные товары"
      pageHeaderProps={{
        extra: (
          <Button
            onClick={() => setIsVisibleCreateModal(!isVisibleCreateModal)}
          >
            Добавить товар
          </Button>
        ),
      }}
    >
      <CreateGoodsModal />
      <Form {...formProps}>
        <Table {...tableProps} rowKey="id">
          <Table.Column<definitions["Goods"]>
            key="poster"
            dataIndex="poster"
            title="Изображение"
            render={(value, record): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Form.Item name="poster" style={{ margin: 0 }}>
                    <Upload
                      maxCount={1}
                      beforeUpload={(file) => {
                        supabaseClient.storage
                          .from("all")
                          .upload(Date.now() + file.name, file, {
                            cacheControl: "3600",
                            upsert: false,
                          })
                          .then((response) => {
                            formProps?.form?.setFieldsValue({
                              poster: `https://ibhhhcvdzzirntcttmso.supabase.in/storage/v1/object/public/${response.data?.Key}`,
                            });
                          });
                        return false;
                      }}
                    >
                      <Button>Загрузить картинку</Button>
                    </Upload>
                  </Form.Item>
                );
              }
              return (
                <img
                  src={value}
                  alt={record.name}
                  style={{ width: 100, height: 100 }}
                />
              );
            }}
          />
          <Table.Column<definitions["Goods"]>
            key="name"
            dataIndex="name"
            title="Название"
            render={(value, record): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Form.Item name="name" style={{ margin: 0 }}>
                    <Input defaultValue={value} />
                  </Form.Item>
                );
              }
              return <TextField value={value} />;
            }}
          />
          <Table.Column<definitions["Goods"]>
            key="description"
            dataIndex="description"
            title="Описание"
            render={(value, record): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Form.Item name="description" style={{ margin: 0 }}>
                    <Input defaultValue={value} />
                  </Form.Item>
                );
              }
              return <TextField value={value} />;
            }}
          />
          <Table.Column<definitions["Goods"]>
            key="price"
            dataIndex="price"
            title="Цена"
            render={(value, record): React.ReactNode => {
              if (isEditing(record.id)) {
                return (
                  <Form.Item name="price" style={{ margin: 0 }}>
                    <Input defaultValue={value} />
                  </Form.Item>
                );
              }
              return <TextField value={value} />;
            }}
          />
          <Table.Column<definitions["Goods"]>
            title="Actions"
            dataIndex="actions"
            key="actions"
            render={(_text, record) => {
              if (isEditing(record.id)) {
                return (
                  <Space>
                    <SaveButton {...saveButtonProps} size="small" />
                    <Button {...cancelButtonProps} size="small">
                      Отмена
                    </Button>
                  </Space>
                );
              }
              return (
                <Space>
                  <EditButton {...editButtonProps(record.id)} size="small" />
                </Space>
              );
            }}
          />
        </Table>
      </Form>
    </List>
  );
};
