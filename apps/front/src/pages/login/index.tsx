import { AntdLayout, Icon, Typography, useAuthenticated, useLogin, useNavigation } from "@pankod/refine";
import { Button, Input, Form, Row, Col } from "antd";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Redirect } from "react-router-dom";
import { discord, form, gitlab, leftBlock, row, video, wrapper } from "./index.styles";

export const LoginPage: React.FC = () => {
	const { mutate: login, isLoading } = useLogin();
	const { replace, push } = useNavigation();
	const { isSuccess } = useAuthenticated();
	const { t } = useTranslation();
	useEffect(() => {
		if (isSuccess) {
			replace('/profile');
		}
	}, [isSuccess]);
	return (
		<AntdLayout
			className={wrapper()}
		>
			<video playsInline autoPlay muted loop poster="bg.jpg" id="bgvid" className={video()}>
				<source src="bg.mp4" type="video/mp4" />
			</video>
			<Row gutter={16} className={row()}>
				<Col span={12} xs={0} sm={0} md={12} xl={12} xxl={12}>
				</Col>
				<Col span={12} xs={24} sm={24} md={12} xl={12} xxl={12} className={leftBlock()}>
					<Typography.Title level={2} style={{ textAlign: 'center' }}>Войти</Typography.Title>
					<Form
						layout="vertical"
						onFinish={(values) => login(values)}
						className={form()}
						size="large"
						style={{ padding: '20px' }}
					>
						<Form.Item name="email" label="Email">
							<Input type='email' placeholder="Введите email" />
						</Form.Item>
						<Form.Item name="password" label="Пароль">
							<Input type='password' placeholder="Введите пароль" />
						</Form.Item>
						<Row gutter={16}>
							<Col span={12}>
								<Button
									htmlType="submit"
									type="primary"
									size="large"
									block
									loading={isLoading}
								>
									{t('pages.login.signin')}
								</Button>
							</Col>
							<Col span={12}>
								<Button
									type="default"
									onClick={() => push('/signup')}
									size="large"
									block
								>
									{t('pages.login.signup')}
								</Button>
							</Col>
							<Col span={24} style={{ marginTop: 16, textAlign: 'center' }}>
								ИЛИ
							</Col>
							<Col span={24} style={{ marginTop: 16, textAlign: 'center' }}>
								<Button
									type="primary"
									size="large"
									icon={<Icon component={() => <img src="discord.png" style={{ width: 40, height: 40, marginTop: '-7px' }} />} />}
									className={discord()}
									onClick={() => {
										login({ provider: 'discord' });
									}}
								>
									Discord
								</Button>
								<Button
									type="primary"
									size="large"
									icon={<Icon component={() => <img src="gitlab.png" style={{ width: 40, height: 40, marginTop: '-7px' }} />} />}
									className={gitlab()}
									onClick={() => {
										login({ provider: 'gitlab' });
									}}
								>
									GitLab
								</Button>
								<Button
									type="primary"
									size="large"
									icon={<Icon component={() => <img src="github.png" style={{ width: 40, height: 40, marginTop: '-7px' }} />} />}
									className={gitlab()}
									onClick={() => {
										login({ provider: 'github' });
									}}
								>
									GitHub
								</Button>
							</Col>
						</Row>
					</Form>
				</Col>
			</Row>
		</AntdLayout>
	);
};