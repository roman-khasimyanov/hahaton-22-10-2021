import { Show, useOne, useShow } from '@pankod/refine';
import { User } from '@supabase/gotrue-js';
import { Col, Divider, Row, Typography } from 'antd';
import { NewsComments } from 'components/NewsComments';
import RichTextEditor from 'components/RichTextViewer';
import { definitions } from 'interfaces/supabase';
import React from 'react';

const NewsShow = () => {
  const { queryResult } = useShow<definitions['News']>({
    resource: 'News'
  });
  console.log(queryResult);
  return (
    <Show
      title={queryResult.data?.data.title}
      pageHeaderProps={{
        extra: (
          <>

          </>
        )
      }}
    >
      <Row>
        <Col span={24}>
          <RichTextEditor
            initialValue={queryResult.data?.data.text}
          />
        </Col>
        <Divider />
        <Col span={24}>
          {queryResult.data?.data.id && <NewsComments id={queryResult.data?.data.id ?? ''} />}
        </Col>
      </Row>
    </Show>
  )
};

export { NewsShow };
