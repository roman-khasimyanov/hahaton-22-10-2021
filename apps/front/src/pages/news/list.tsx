import {
  List,
  useList,
  Row,
  Col,
  Skeleton,
  Divider,
  Select,
  TagField,
} from "@pankod/refine";
import { useUser } from "hooks/useUser";
import { definitions } from "interfaces/supabase";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { NewsCard } from "./NewsCard";
import "./index.css";

export const News = () => {
  const { t } = useTranslation();
  const user = useUser();
  const [selectedTopicIds, setSelectedTopicIds] = useState<
    string[] | undefined
  >([]);
  const { data: newsData, isLoading: isLoadingNews } = useList<
    definitions["News"]
  >({
    resource: "News",
    config: {
      filters: [
        {
          field: "topic_id",
          value: selectedTopicIds,
          operator: "in",
        },
      ],
    },
  });
  const { data: newsTopicData, isLoading: isLoadingNewsTopic } = useList<
    definitions["NewsTopic"]
  >({
    resource: "NewsTopic",
  });

  useEffect(() => {
    setSelectedTopicIds(newsTopicData?.data.map((newsTopic) => newsTopic.id));
  }, [newsTopicData]);

  useEffect(() => {
    console.info({ selectedTopicIds });
  }, [selectedTopicIds]);

  if (isLoadingNews || isLoadingNewsTopic) {
    return <Skeleton />;
  }

  return (
    <List title={t("news.news")} canCreate={user?.is_admin}>
      <Row gutter={16}>
        <Row justify="space-between" style={{ width: "100%" }}>
          <Col
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <Select defaultValue="title">
              <Select.Option value="title">По наименованию</Select.Option>
              <Select.Option value="rating">По популярности</Select.Option>
            </Select>
            <div>
              {newsTopicData?.data.map((newsTopic, index) => {
                const isCurrentTopicDisabled = selectedTopicIds?.find(
                  (topicId) => topicId === newsTopic.id
                );

                return (
                  <TagField
                    className={
                      isCurrentTopicDisabled ? "tag" : "tag disabled__tag"
                    }
                    value={newsTopic.name}
                    color={index % 2 === 0 ? "pink" : "blue"}
                    style={{ marginBottom: 16 }}
                    onClick={() => {
                      const isTopicAlreadyExists = selectedTopicIds?.includes(
                        newsTopic.id
                      );

                      if (isTopicAlreadyExists) {
                        setSelectedTopicIds((prev) =>
                          prev?.filter((id) => id !== newsTopic.id)
                        );
                      } else {
                        setSelectedTopicIds((prev) => {
                          if (prev) {
                            const newPrev = [...prev];
                            newPrev.push(newsTopic.id);

                            return newPrev;
                          }
                          return prev;
                        });
                      }
                    }}
                  />
                );
              })}
            </div>
          </Col>
        </Row>
        <Divider />
        {newsData?.data.map((news) => (
          <Col span={24}>
            <NewsCard
              key={news.id}
              news={news}
              topic={newsTopicData?.data.find(
                (topic) => topic.id === news.topic_id
              )}
            />
            <Divider />
          </Col>
        ))}
      </Row>
    </List>
  );
};
