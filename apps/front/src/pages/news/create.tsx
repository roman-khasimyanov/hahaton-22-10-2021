import {
  Create,
  Form,
  Input,
  useForm,
  useList,
  Select,
  Spin,
} from "@pankod/refine";
import RichTextEditor from "components/RichTextEditor";
import { useUser } from "hooks/useUser";
import { definitions } from "interfaces/supabase";
import { useTranslation } from "react-i18next";

const COMMON_TOPIC_ID = "577e5381-7277-4010-8924-f1444ac554ce";

export const NewsCreate = () => {
  const { t } = useTranslation();
  const currentUser = useUser();
  const { formProps, saveButtonProps } = useForm<definitions["News"]>({
    resource: "News",
  });
  const { isLoading, data } = useList<definitions["NewsTopic"]>({
    resource: "NewsTopic",
  });

  if (!currentUser) {
    return <Spin />;
  }

  return (
    <Create saveButtonProps={saveButtonProps} title={t("news.titles.create")}>
      <Form {...formProps} layout="vertical">
        <Form.Item
          initialValue={data?.data[0].id ?? COMMON_TOPIC_ID}
          label={t("news.fields.topic")}
          name="topic_id"
        >
          <Select loading={isLoading}>
            {data?.data.map((topic) => (
              <Select.Option key={topic.id} value={topic.id}>
                {topic.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label={t("news.fields.title")} name="title">
          <Input />
        </Form.Item>
        <Form.Item label={t("idea.fields.text")} name="text" valuePropName="initialValue">
          <RichTextEditor />
        </Form.Item>
        <Form.Item
          style={{ display: "none" }}
          name="created_by"
          initialValue={currentUser?.auth_user_id}
        >
          <Input />
        </Form.Item>
      </Form>
    </Create>
  );
};
