import React, { useEffect } from "react";
import {
  Card,
  Link,
  Row,
  useList,
  Divider,
  useCreate,
  useDelete,
  TagField,
  Skeleton,
} from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { UserById } from "components/User/UserById";
import { FavoriteButton } from "components/FavoriteButton";
import { useUser } from "hooks/useUser";
import { CountLike } from "components/CountLike";
import RichTextViewer from "components/RichTextViewer";

export type NewsCardProps = {
  news: definitions["News"];
  topic?: definitions["NewsTopic"];
};

export const NewsCard: React.FC<NewsCardProps> = ({
  news: { id, created_by, text, title, created_at, topic_id },
  topic,
}) => {
  const currentUser = useUser();
  const { data: newsLikeData, refetch: refetchLikes } = useList<
    definitions["NewsLike"]
  >({
    resource: "NewsLike",
    config: { pagination: { pageSize: 10000 } },
  });
  const { data: newsFavoriteData, refetch: refetchFavorites } = useList<
    definitions["NewsFavorite"]
  >({
    resource: "NewsFavorite",
    config: {
      pagination: { pageSize: 10000 },
      filters: [
        {
          field: "user_id",
          value: currentUser?.auth_user_id,
          operator: "eq",
        },
      ],
    },
  });
  const { data: newsCommentData } = useList<definitions["NewsComment"]>({
    resource: "NewsComment",
    config: { pagination: { pageSize: 10000 } },
  });
  const countOfLike = newsLikeData?.data.filter(
    (newsLike) => newsLike.news_id === id
  ).length;
  const like = newsLikeData?.data.find(
    (newsLike) =>
      newsLike.news_id === id && newsLike.user_id === currentUser?.auth_user_id
  );
  const isLiked = like !== undefined;
  const favorite = newsFavoriteData?.data.find(
    (newsFavorite) =>
      newsFavorite.user_id === currentUser?.auth_user_id &&
      newsFavorite.news_id === id
  );
  const isFavorite = favorite !== undefined;
  const countOfComment = newsCommentData?.data.filter(
    (newsComment) => newsComment.news_id === id
  ).length;
  const { mutate: deleteMutate } = useDelete();
  const { mutate: createMutate } = useCreate();

  const Title = (
    <div style={{ display: "flex", justifyContent: "space-between" }}>
      <div style={{ fontSize: 28 }}>
        <Link to={`/news/show/${id}`}>{title}</Link>
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "end",
          flexDirection: "column",
        }}
      >
        <UserById id={created_by} />
        <div style={{ marginRight: 8 }}>
          {new Date(created_at!).toLocaleDateString()}
        </div>
      </div>
    </div>
  );

  const onClickFavorite = (value: boolean) => {
    if (value) {
      createMutate({
        resource: "NewsFavorite",
        values: {
          news_id: id,
          user_id: currentUser?.auth_user_id,
        },
      });
    } else if (isFavorite) {
      deleteMutate({
        resource: "NewsFavorite",
        id: favorite?.id ?? "",
      });
    }
  };

  const onClickLike = (value: boolean) => {
    if (value) {
      createMutate({
        resource: "NewsLike",
        values: {
          news_id: id,
          user_id: currentUser?.auth_user_id,
        },
      });
    } else if (isLiked) {
      deleteMutate({
        resource: "NewsLike",
        id: like?.id ?? "",
      });
    }
  };

  useEffect(() => {
    refetchFavorites?.();
    refetchLikes?.();
  }, [currentUser, refetchFavorites, refetchLikes]);
  if (currentUser && newsLikeData && newsFavoriteData) {
    return (
      <Card
        title={Title}
        style={{
          boxShadow:
            "rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px",
        }}
      >
        <TagField
          value={topic?.name}
          color="pink"
          style={{ marginBottom: 16 }}
        />
        <RichTextViewer initialValue={text} />
        <Divider />
        <Row className="icons__list">
          <FavoriteButton onClick={onClickFavorite} favorite={isFavorite} />
          <CountLike
            isLiked={isLiked}
            count={countOfLike ?? 0}
            onClick={onClickLike}
          />
          <span style={{ marginLeft: "auto", color: "rgb(133, 129, 129)" }}>
            Комментарии: {countOfComment}
          </span>
        </Row>
      </Card>
    );
  } else {
    return <Skeleton />;
  }
};
