import {
  Create,
  Form,
  Input,
  Select,
  useForm,
  useSelect,
  useTranslate,
} from "@pankod/refine";
import { Spin } from "antd";
import RichTextEditor from "components/RichTextEditor";
import { useUser } from "hooks/useUser";
import { definitions } from "interfaces/supabase";
import { useTranslation } from "react-i18next";

export const IdeaCreate = () => {
  const { formProps, saveButtonProps } = useForm<definitions["Idea"]>({
    resource: "Idea",
  });
  const { t } = useTranslation();
  const user = useUser();

  return (
    <>
      {user ? (
        <Create saveButtonProps={saveButtonProps} title="Добавить идею">
          <Form {...formProps} layout="vertical">
            <Form.Item label={t("idea.fields.title")} name="title">
              <Input />
            </Form.Item>
            <Form.Item label={t("idea.fields.text")} name="text" valuePropName="initialValue">
              <RichTextEditor />
            </Form.Item>
            <Form.Item
              initialValue={user?.auth_user_id}
              name="created_by"
              style={{ display: "none" }}
            >
              <Input />
            </Form.Item>
          </Form>
        </Create>
      ) : (
        <Spin />
      )}
    </>
  );
};
