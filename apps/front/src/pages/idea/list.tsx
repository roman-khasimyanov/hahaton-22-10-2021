import {
  List,
  Select,
  Col,
  Row,
  useList,
  Skeleton,
  Divider,
  Link,
} from "@pankod/refine";
import { List as AntdList, Avatar, Space, Spin } from "antd";
import "./List.css";
import { UserById } from "components/User/UserById";
import { definitions } from "interfaces/supabase";
import { useUser } from "hooks/useUser";
import dayjs from "dayjs";
import { IdeaFavoriteButton } from "./IdeaFavoriteButton";
import { useEffect, useState } from "react";
import { IdeaLikes } from "./IdeaLikes";
import RichTextViewer from "components/RichTextViewer";
import { useTranslation } from "react-i18next";
const { Option } = Select;

export const Ideas: React.FC = () => {
  const { t } = useTranslation();
  const user = useUser();
  const [sorting, setSorting] = useState<"title" | "created_at">("title");
  const { data: ideas, isLoading: isLoadingIdeas } = useList<
    definitions["Idea"]
  >({
    resource: "Idea",
    config: {
      sort: [{ field: sorting, order: "asc" }],
    },
  });

  const {
    data: favoriteIdeas,
    isLoading: isLoadingFavoriteIdeas,
    refetch: refetchFavorites,
  } = useList<definitions["IdeaFavorite"]>({
    resource: "IdeaFavorite",
    config: {
      pagination: {
        pageSize: 10000,
      },
      filters: [
        {
          field: "user_id",
          value: user?.auth_user_id,
          operator: "eq",
        },
      ],
    },
  });

  const {
    data: ideaLikes,
    isLoading: isLoadingIdeaLikes,
    refetch: refetchLikes,
  } = useList<definitions["IdeaLike"]>({
    resource: "IdeaLike",
    config: {
      pagination: {
        pageSize: 10000,
      },
    },
  });

  const { data: ideasCommentData } = useList<definitions["IdeaComments"]>({
    resource: "IdeaComments",
    config: { pagination: { pageSize: 10000 } },
  });

  useEffect(() => {
    refetchFavorites?.();
    refetchLikes?.();
  }, [user, refetchFavorites, refetchLikes]);
  if (user && favoriteIdeas && ideas && ideaLikes) {
    return (
      <List title={t("idea.idea")}>
        <Col>
          <Row justify="start">
            <Select defaultValue="title" onChange={(v) => setSorting(v)}>
              <Option value="title">По наименованию</Option>
              <Option value="created_at">По дате создания</Option>
            </Select>
          </Row>
          <Divider />
          <Row>
            <AntdList
              dataSource={ideas?.data}
              renderItem={(item) => {
                const likes = ideaLikes.data.filter(
                  (like) => like.idea_id === item.id
                );
                const countOfComment = ideasCommentData?.data.filter(
                  (ideasComment) => ideasComment.idea_id === item.id
                ).length;
                return (
                  <AntdList.Item className="list__item">
                    <Col style={{ width: "100%" }}>
                      <Row justify="space-between" align="middle">
                        <Link
                          className="list__title"
                          style={{ color: "#E0529C" }}
                          to={`/idea/show/${item.id}`}
                        >
                          {item.title}
                        </Link>
                        <div className="data__item">
                          {dayjs(item.created_at).format("L")}
                        </div>
                      </Row>
                      {item.created_by && <UserById id={item.created_by} />}
                      <div className="list__value">
                        <RichTextViewer initialValue={item.text} />
                      </div>

                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Row
                          style={{
                            display: "flex",
                          }}
                        >
                          <IdeaFavoriteButton
                            ideaFavoriteId={
                              favoriteIdeas?.data.find(
                                (currentElement) =>
                                  item.id === currentElement.idea_id
                              )?.id
                            }
                            ideaId={item.id}
                            userId={user?.auth_user_id}
                          />
                          <IdeaLikes
                            userId={user?.auth_user_id}
                            ideaId={item.id}
                            ideaLikeId={
                              likes.find(
                                (currentElement) =>
                                  user.auth_user_id === currentElement.user_id
                              )?.id
                            }
                            countLike={likes.length}
                          />
                        </Row>
                        <div style={{ color: "rgb(133, 129, 129)" }}>
                          Комментарии: {countOfComment}
                        </div>
                      </div>
                    </Col>
                  </AntdList.Item>
                );
              }}
            />
          </Row>
        </Col>
      </List>
    );
  } else {
    return <Skeleton />;
  }
};
