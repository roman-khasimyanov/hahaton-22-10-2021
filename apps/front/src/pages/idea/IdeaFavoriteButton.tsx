import React from "react";
import { useCreate, useDelete } from "@pankod/refine";
import { FavoriteButton } from "components/FavoriteButton";

type IdeaFavoriteButtonProps = {
  ideaFavoriteId?: string;
  userId: string;
  ideaId: string;
};

export const IdeaFavoriteButton = ({
  ideaFavoriteId,
  ideaId,
  userId,
}: IdeaFavoriteButtonProps) => {
  const { mutate: createMutate } = useCreate();
  const { mutate: deleteMutate } = useDelete();
  return (
    <FavoriteButton
      favorite={ideaFavoriteId !== undefined}
      onClick={(value) => {
        if (value) {
          createMutate({
            resource: "IdeaFavorite",
            values: {
              idea_id: ideaId,
              user_id: userId,
            },
          });
        } else if (ideaFavoriteId) {
          deleteMutate({
            resource: "IdeaFavorite",
            id: ideaFavoriteId,
          });
        }
      }}
    />
  );
};
