import { Show, Skeleton, useList, useOne, useShow } from '@pankod/refine';
import { User } from '@supabase/gotrue-js';
import { Col, Divider, Row, Typography } from 'antd';
import { IdeaComments } from 'components/IdeaComments';
import { useUser } from 'hooks/useUser';
import { definitions } from 'interfaces/supabase';
import React, { useEffect, useMemo } from 'react';
import { IdeaFavoriteButton } from './IdeaFavoriteButton';
import { IdeaLikes } from './IdeaLikes';

const ShowIdea = () => {
  const user = useUser();
  const { queryResult } = useShow<definitions['Idea']>({
    resource: 'Idea'
  });
  const {
    data: favoriteIdeas,
    isLoading: isLoadingFavoriteIdeas,
    refetch: refetchFavorites,
  } = useList<definitions["IdeaFavorite"]>({
    resource: "IdeaFavorite",
    config: {
      pagination: {
        pageSize: 10000,
      },
      filters: [
        {
          field: "user_id",
          value: user?.auth_user_id,
          operator: "eq",
        },
      ],
    },
  });

  const {
    data: ideaLikes,
    isLoading: isLoadingIdeaLikes,
    refetch: refetchLikes,
  } = useList<definitions["IdeaLike"]>({
    resource: "IdeaLike",
    config: {
      pagination: {
        pageSize: 10000,
      },
    },
  });
  useEffect(() => {
    refetchFavorites?.();
    refetchLikes?.();
  }, [user, refetchFavorites, refetchLikes]);
  const likes = useMemo(() => ideaLikes?.data.filter(
    (like) => like.idea_id === queryResult.data?.data.id
  ) ?? [], [ideaLikes, queryResult.data?.data.id]);
  if (user && queryResult.data?.data.id) {
    return (
      <Show
        title={queryResult.data?.data.title}
        pageHeaderProps={{
          extra: (
            <>

            </>
          )
        }}
      >
        <Row>
          <Col span={24}>
            {queryResult.data?.data.text}
            <Row>
              <IdeaFavoriteButton
                ideaFavoriteId={
                  favoriteIdeas?.data.find(
                    (currentElement) =>
                      queryResult.data?.data.id === currentElement.idea_id
                  )?.id
                }
                ideaId={queryResult.data?.data.id}
                userId={user?.auth_user_id}
              />
              <IdeaLikes
                userId={user?.auth_user_id}
                ideaId={queryResult.data?.data.id}
                ideaLikeId={
                  likes.find(
                    (currentElement) =>
                      user.auth_user_id === currentElement.user_id
                  )?.id
                }
                countLike={likes.length}
              />
            </Row>
          </Col>
          <Divider />
          <Col span={24}>
            {queryResult.data?.data.id && <IdeaComments id={queryResult.data?.data.id ?? ''} />}
          </Col>
        </Row>
      </Show>
    )
  } else {
    return <Skeleton />;
  }
};

export { ShowIdea };
