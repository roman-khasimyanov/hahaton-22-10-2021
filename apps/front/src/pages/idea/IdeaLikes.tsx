import React from "react";
import { useCreate, useDelete } from "@pankod/refine";
import { CountLike } from "../../components/CountLike";

type IdeaLikesProps = {
  ideaLikeId?: string;
  userId: string;
  ideaId: string;
  countLike?: number;
};

export const IdeaLikes = ({
  ideaLikeId,
  ideaId,
  userId,
  countLike,
}: IdeaLikesProps) => {
  const { mutate: createMutate } = useCreate();
  const { mutate: deleteMutate } = useDelete();
  return (
    <CountLike
      isLiked={ideaLikeId !== undefined}
      count={countLike ?? 0}
      onClick={(value) => {
        if (value) {
          createMutate({
            resource: "IdeaLike",
            values: {
              idea_id: ideaId,
              user_id: userId,
            },
          });
        } else if (ideaLikeId) {
          deleteMutate({
            resource: "IdeaLike",
            id: ideaLikeId,
          });
        }
      }}
    />
  );
};
