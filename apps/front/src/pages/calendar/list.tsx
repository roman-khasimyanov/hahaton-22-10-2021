import React, { useState } from "react";
import {
  Badge,
  BadgeProps,
  Calendar,
  List,
  Skeleton,
  Spin,
  useGetLocale,
  useList,
} from "@pankod/refine";
import dayjs, { Dayjs, locale } from "dayjs";
import enLocale from "antd/es/date-picker/locale/en_US";
import ruLocale from "antd/es/date-picker/locale/ru_RU";
import { useTranslation } from "react-i18next";
import { definitions } from "interfaces/supabase";
import dayOfYear from 'dayjs/plugin/dayOfYear';
import './index.css';
import { ModalEvent } from "./ModalEvent";

dayjs.extend(dayOfYear)

export const UserCalendar = () => {
  const { t } = useTranslation();
  const [selectedDate, setSelectedDate] = useState<Dayjs>(dayjs());
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const { data: eventsData, isLoading: isLoadingEvents } = useList<
    definitions["Event"]
  >({
    resource: "Event",
    config: {
      filters: [
        { field: 'from_date', operator: 'gte', value: dayjs(selectedDate).startOf('M') },
        { field: 'to_date', operator: 'lte', value: dayjs(selectedDate).endOf('M') }
      ],
    },
  });
  const locale = useGetLocale();
  const getListData = (value: Dayjs) => {
    const date = value.dayOfYear();
    return eventsData?.data.filter((ev) => dayjs(ev.from_date).dayOfYear() === date)
      .map((ev) => ({
        type: 'success',
        content: ev.title,
      })) ?? []
  };

  function dateCellRender(value: Dayjs) {
    const listData = getListData(value);
    return (
      <div>
        {listData.map((item) => (
          <Badge
            status={item.type as BadgeProps["status"]}
            text={item.content}
          />
        ))}
      </div>
    );
  }


  return (
    <List title={t("calendar.calendar")}>
      {isLoadingEvents ? <Skeleton /> : (
        <>
          <Calendar
            className="user-calendar"
            locale={locale() === "en" ? enLocale : ruLocale}
            dateCellRender={dateCellRender}
            defaultValue={selectedDate}
            onChange={setSelectedDate}
            mode="month"
            onSelect={(date) => {
              setSelectedDate(date);
              setIsOpenModal(true);
            }}
          />
          <ModalEvent
            isOpen={isOpenModal}
            onCancel={() => setIsOpenModal(false)}
            eventList={eventsData?.data.filter((ev) => dayjs(ev.from_date).dayOfYear() === selectedDate.dayOfYear()) ?? []}
            date={selectedDate}

          />
        </>
      )}
    </List>
  );
};
