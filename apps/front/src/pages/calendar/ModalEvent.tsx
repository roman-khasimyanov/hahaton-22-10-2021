import { Modal } from '@pankod/refine';
import dayjs, { Dayjs } from 'dayjs';
import { Col, List as AntdList, Row } from 'antd';
import React from 'react';
import { definitions } from 'interfaces/supabase';

type ModalEventProps = {
  eventList: Array<definitions["Event"]>;
  date: Dayjs;
  isOpen: boolean;
  onCancel: () => void;
};

export const ModalEvent = ({ date, eventList, isOpen, onCancel }: ModalEventProps) => {
  return (
    <Modal
      className="calendar-modal"
      title={`События ${dayjs(date).format('L')}`} visible={isOpen} onCancel={onCancel} footer={null}>
      <AntdList
        dataSource={eventList ?? []}
        renderItem={(item) => (
          <AntdList.Item>
            <Row align="middle">
              <Col style={{ color: 'gray' }}>
                {dayjs(item.from_date).get('hour')} : {dayjs(item.from_date).get('minute')}
                <span style={{ margin: '0 6px 0 6px' }} >-</span>
                {dayjs(item.to_date).get('hour')} : {dayjs(item.to_date).get('minute')}
              </Col>
              <Col style={{ marginLeft: '24px' }}>
                <Row style={{ color: '#E0529C' }}>
                  {item.title}
                </Row>
                <Row>
                  {item.text}
                </Row>
              </Col>
            </Row>
          </AntdList.Item>
        )}
      >
      </AntdList>
    </Modal>
  );
}