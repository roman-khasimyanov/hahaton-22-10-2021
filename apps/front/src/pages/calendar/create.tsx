import { MinusOutlined } from "@ant-design/icons";
import { Create, Form, Input, useForm, DatePicker, Select, Row } from "@pankod/refine";
import { useUser } from "hooks/useUser";
import { definitions } from "interfaces/supabase";
import { useTranslation } from "react-i18next";

export const EventCreate = () => {
  const { t } = useTranslation();
  const { formProps, saveButtonProps } = useForm<definitions["Event"]>({
    resource: "Event",
  });

  const currentUser = useUser();

  return (
    <Create saveButtonProps={saveButtonProps} title={t("calendar.titles.create")}>
      <Form {...formProps} layout="vertical" >
        <Form.Item label={t("calendar.fields.title")} name="title">
          <Input />
        </Form.Item>
        <Form.Item label={t("calendar.fields.text")} name="text">
          <Input.TextArea />
        </Form.Item>
        <Row align="middle">
          <Form.Item label={t("calendar.fields.from_date")} name="from_date">
            <DatePicker showTime />
          </Form.Item>
          <MinusOutlined style={{ margin: '0 12px 0 12px' }} />
          <Form.Item label={t("calendar.fields.to_date")} name="to_date">
            <DatePicker showTime />
          </Form.Item>
        </Row>
        <Form.Item name="created_by" initialValue={currentUser?.id}>
          <Input style={{ display: 'none' }} />
        </Form.Item>
      </Form>
    </Create>
  );
};
