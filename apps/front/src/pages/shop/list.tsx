import {
  Icon,
  List,
  TagField,
  useList,
  useMany,
  useCreate,
  useUpdate,
} from "@pankod/refine";
import {
  Button,
  Card,
  Col,
  Image,
  notification,
  Row,
  Skeleton,
  Space,
  Typography,
} from "antd";
import { useUser } from "hooks/useUser";
import { definitions } from "interfaces/supabase";
import React from "react";

export const ShopList = () => {
  const { data } = useList<definitions["Goods"]>({
    resource: "Goods",
    config: {
      pagination: {
        pageSize: 10000,
      },
    },
  });

  const { data: topicsData } = useList<definitions["GoodsTopic"]>({
    resource: "GoodsTopic",
  });

  const user = useUser();
  const { data: userPurchases } = useList<definitions["Purchases"]>({
    resource: "Purchases",
    config: {
      filters: [
        {
          field: "user_id",
          operator: "eq",
          value: user?.auth_user_id,
        },
      ],
      pagination: { pageSize: 10000 },
    },
  });
  const { mutate } = useCreate<definitions["Purchases"]>();
  const { mutate: mutateProfile } = useUpdate<definitions["profile"]>();
  console.info(topicsData);
  if (!user) return <Skeleton />;
  return (
    <List title="Магазин">
      <Row gutter={16}>
        {data?.data.map((good) => (
          <Col span={8} style={{ marginTop: 16 }}>
            <Card
              actions={
                !userPurchases?.data.find((p) => p.good_id === good.id)
                  ? [
                    <Button
                      icon={
                        <Icon
                          component={() => (
                            <img
                              src="/currency.png"
                              style={{
                                width: 32,
                                height: 32,
                                marginTop: "-7px",
                              }}
                              alt="currency"
                            />
                          )}
                        />
                      }
                      type="link"
                      onClick={() => {
                        if ((user?.currency ?? 0) < good.price) {
                          notification.error({
                            message: "Недостаточно средств :C",
                          });
                        } else {
                          mutate({
                            resource: "Purchases",
                            values: {
                              user_id: user?.auth_user_id,
                              good_id: good.id,
                            },
                          });
                          mutateProfile({
                            resource: "profile",
                            id: user?.id ?? "",
                            values: {
                              currency: user.currency - good.price,
                            },
                          });
                        }
                      }}
                    >
                      {good.price}
                    </Button>,
                  ]
                  : [
                    <Typography.Text type="secondary">
                      Куплено
                    </Typography.Text>,
                  ]
              }
              title={
                <Space
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <Typography.Text>{good.name}</Typography.Text>
                  <TagField
                    value={topicsData?.data.find((t) => good.topic_id)?.name}
                    color="pink"
                    style={{ marginLeft: "8px" }}
                  />
                </Space>
              }
              cover={
                <Image
                  alt="example"
                  style={{ maxHeight: 200, objectFit: "contain", margin: 16 }}
                  src={good.poster}
                />
              }
            >
              <Typography.Text>{good.description}</Typography.Text>
            </Card>
          </Col>
        ))}
      </Row>
    </List>
  );
};
