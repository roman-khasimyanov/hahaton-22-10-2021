import { styleConfig } from "theme";

export const globalStyles = styleConfig.globalCss({
  '@font-face': [
    {
      fontFamily: 'Golos',
      src: 'local("Golos"), '
        + 'url("/fonts/Golos/Golos-Text_Regular.woff") format("woff"), '
        + 'url("/fonts/Golos/Golos-Text_Regular.woff2") format("woff2"), '
        + 'url("/fonts/Golos/Golos-UI_Regular.ttf") format("ttf")',
      fontWeight: 300,
      fontStyle: 'normal',
    },
    {
      fontFamily: 'Golos',
      src: 'local("Golos"), '
        + 'url("/fonts/Golos/Golos-Text_Medium.woff") format("woff"), '
        + 'url("/fonts/Golos/Golos-Text_Medium.woff2") format("woff2"), '
        + 'url("/fonts/Golos/Golos-UI_Medium.ttf") format("ttf")',
      fontWeight: 400,
      fontStyle: 'normal',
    },
    {
      fontFamily: 'Golos',
      src: 'local("Golos"), '
        + 'url("/fonts/Golos/Golos-Text_DemiBold.woff") format("woff"), '
        + 'url("/fonts/Golos/Golos-Text_DemiBold.woff2") format("woff2"), '
        + 'url("/fonts/Golos/Golos-UI_Medium.ttf") format("ttf")',
      fontWeight: 500,
      fontStyle: 'normal',
    },
    {
      fontFamily: 'Golos',
      src: 'local("Golos"), '
        + 'url("/fonts/Golos/Golos-Text_Bold.woff") format("woff"), '
        + 'url("/fonts/Golos/Golos-Text_Bold.woff2") format("woff2"), '
        + 'url("/fonts/Golos/Golos-UI_Bold.ttf") format("ttf")',
      fontWeight: 600,
      fontStyle: 'normal',
    },
    {
      fontFamily: 'Golos',
      src: 'local("Golos"), '
        + 'url("/fonts/Golos/Golos-Text_Black.woff") format("woff"), '
        + 'url("/fonts/Golos/Golos-Text_Black.woff2") format("woff2"), '
        + 'url("/fonts/Golos/Golos-UI_Black.ttf") format("ttf")',
      fontWeight: 700,
      fontStyle: 'normal',
    },
  ],
  body: {
    fontFamily: 'Golos',
  },
});