import { AuthProvider, useNavigation } from "@pankod/refine";
import { supabaseClient } from "./supabaseClient";

export const authProvider: AuthProvider = {
  login: async ({ email, password, provider }) => {
    const { user, error } = await supabaseClient.auth.signIn(
      provider
        ? { provider }
        : {
            email,
            password,
          }
    );

    if (error) {
      return Promise.reject(error);
    }

    if (user) {
      return Promise.resolve({ redirectPath: "/" });
    }
  },
  logout: async () => {
    const { error } = await supabaseClient.auth.signOut();
    if (error) {
      return Promise.reject(error);
    }

    return Promise.resolve("/");
  },
  checkError: () => Promise.resolve(),
  checkAuth: () => {
    return new Promise((res, rej) => {
      setTimeout(() => {
        const session = supabaseClient.auth.session();
        if (session) {
          return res();
        }

        return rej();
      }, 100);
    });
  },
  getPermissions: async () => {
    const user = supabaseClient.auth.user();

    if (user) {
      return Promise.resolve(user.role);
    }
  },
  getUserIdentity: async () => {
    const user = supabaseClient.auth.user();

    if (user) {
      return Promise.resolve({
        ...user,
        name: user.email,
      });
    }
  },
};
