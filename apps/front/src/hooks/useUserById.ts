import { useList } from "@pankod/refine";
import { definitions } from "interfaces/supabase";

export const useUserById = (userId: string): definitions["profile"] | null => {
  const { data } = useList<definitions["profile"]>({
    resource: "profile",
  });
  const neededUser = data?.data?.find((user) => user.auth_user_id === userId);

  if (!neededUser) {
    return null;
  }

  return neededUser;
};
