import { Remarkable } from "remarkable";

export const useMd = (str: string): string => {
  const md = new Remarkable();
  const htmlInString = md.render(str);

  return htmlInString;
};
