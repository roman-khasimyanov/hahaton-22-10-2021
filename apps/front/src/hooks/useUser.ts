import { useList, useOne } from "@pankod/refine";
import { definitions } from "interfaces/supabase";
import { useEffect, useState } from "react";
import { supabaseClient } from "supabaseClient";

export const useUser = () => {
  const supabaseUser = supabaseClient.auth.user();
  const [user, setUser] = useState<definitions['profile'] | null>(null);

  const { data } = useList<definitions["profile"]>({
    resource: "profile",
    config: {
      filters: [
        {
          field: "auth_user_id",
          operator: "eq",
          value: supabaseUser ? supabaseUser.id : "",
        },
      ],
    },
  });

  useEffect(() => {
    if (data?.data[0]) {
      setUser(data?.data[0]);
    }
  }, [data])

  return user;
};
