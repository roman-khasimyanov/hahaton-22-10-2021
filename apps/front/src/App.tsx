import React from "react";
import { createClient, dataProvider } from "@pankod/refine-supabase";
import {
  AntdLayout,
  Refine,
  LayoutProps,
  Resource,
  AuthProvider,
  useNavigation,
} from "@pankod/refine";
// import "@pankod/refine/dist/styles.min.css";
import "antd/dist/antd.dark.css";
import { LoginPage } from "pages/login";
import { SignupPage } from "pages/signup/create";
import { UsersList } from "pages/user/list";
import { Header } from "components/Header";
import { UserCalendar } from "pages/calendar/list";
import { IdeaCreate } from "pages/idea/create";
import { Ideas } from "pages/idea/list";
import { News } from "pages/news/list";
import { NewsCreate } from "pages/news/create";
import { Sider } from "components/Sider";
import { useTranslation } from "react-i18next";
import { globalStyles } from "App.styles";
import { authProvider } from "authProvider";
import { supabaseClient } from "supabaseClient";
import { Layout } from "components/Layout";
import locale from "antd/lib/locale/ru_RU";
import { Redirect, RouteProps } from "react-router-dom";
import { EventCreate } from "pages/calendar/create";
import { ShowIdea } from "pages/idea/show";
import { ProfileList } from "pages/profile/list";
import { NewsShow } from "pages/news/show";
import { ShopList } from "pages/shop/list";
import { ShoppingTwoTone, SoundTwoTone, BulbTwoTone, CalendarTwoTone, IdcardTwoTone } from '@ant-design/icons';

globalStyles();

const customRoutes: RouteProps[] = [
  {
    exact: true,
    path: "/",
    component: LoginPage,
  },
  {
    exact: true,
    path: "/signup",
    component: SignupPage,
  },
];

const twoToneColor = ['#e0529c', '#303030'] as [string, string];

export const App: React.FC = () => {
  const { t, i18n } = useTranslation();

  const i18nProvider = {
    translate: (key: string, params: object) => t(key, params),
    changeLocale: (lang: string) => i18n.changeLanguage(lang),
    getLocale: () => i18n.language,
  };

  return (
    <Refine
      LoginPage={LoginPage}
      authProvider={authProvider}
      dataProvider={dataProvider(supabaseClient)}
      i18nProvider={i18nProvider}
      Header={Header}
      Sider={Sider}
      Layout={Layout}
      configProviderProps={{
        locale,
      }}
      routes={customRoutes}
    >
      <Resource
        name="profile"
        list={ProfileList}
        icon={<IdcardTwoTone twoToneColor={twoToneColor} />}
      />
      <Resource
        name="calendar"
        list={UserCalendar}
        create={EventCreate}
        icon={<CalendarTwoTone twoToneColor={twoToneColor} />}
      />
      <Resource
        name="idea"
        list={Ideas}
        create={IdeaCreate}
        show={ShowIdea}
        icon={<BulbTwoTone twoToneColor={twoToneColor} />}
      />
      <Resource
        name="news"
        list={News}
        create={NewsCreate}
        show={NewsShow}
        icon={<SoundTwoTone twoToneColor={twoToneColor} />}
      />
      <Resource
        name="shop"
        list={ShopList}
        options={{
          label: 'Магазин'
        }}
        icon={<ShoppingTwoTone twoToneColor={twoToneColor} />}
      />
    </Refine>
  );
};
