import React from 'react';
import { Button, Form } from 'antd';
import { CustomEditor } from 'components/CustomEditor';
import { useUser } from 'hooks/useUser';
import RichTextEditor from 'components/RichTextEditor';
import { useCreate } from '@pankod/refine';
import { definitions } from 'interfaces/supabase';

export type FormCommentProps = {
  idea_id: string;
  comment_id?: string;
  afterSubmit?: () => void;
};

export const FormComment = ({ idea_id, comment_id, afterSubmit }: FormCommentProps) => {
  const user = useUser();
  const { mutate } = useCreate<definitions['IdeaComments']>();
  return (
    <Form
      onFinish={(values) => {
        const val: Partial<definitions['IdeaComments']> = {
          comment_id: comment_id,
          creator_id: user?.auth_user_id,
          idea_id: idea_id,
          text: values.text,
        };
        mutate({
          resource: 'IdeaComments',
          values: val,
        })
        afterSubmit?.();
      }}
    >
      <div style={{ minHeight: '250px', padding: 16 }}>
        <Form.Item name="text" valuePropName="initialValue">
          <RichTextEditor />
        </Form.Item>
      </div>
      <Form.Item>
        <Button type="link" htmlType="submit">
          Сохранить
        </Button>
      </Form.Item>
    </Form>
  )
};