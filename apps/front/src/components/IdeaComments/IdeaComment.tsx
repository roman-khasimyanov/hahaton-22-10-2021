import { useList, useOne } from '@pankod/refine';
import { User } from '@supabase/gotrue-js';
import { definitions } from 'interfaces/supabase';
import { Avatar, Comment, Tooltip, Typography } from 'antd';
import React from 'react';
import dayjs from 'dayjs';
import RichTextViewer from 'components/RichTextViewer';

export type IdeaCommentProps = {
  comment: definitions['IdeaComments'];
};

export const IdeaComment = ({ comment }: IdeaCommentProps) => {
  const { data } = useList<definitions['profile']>({
    config: {
      filters: [{ field: 'auth_user_id', value: comment.creator_id, operator: 'eq' }],
    },
    resource: 'profile',
  });
  return (
    <Comment
      author={<Typography.Text type="secondary">{data?.data[0]?.full_name ?? ''}</Typography.Text>}
      avatar={<Avatar src={data?.data[0]?.avatar_url ?? ''} />}
      content={
        <RichTextViewer initialValue={comment.text} />
      }
      datetime={
        <Tooltip title={dayjs().format('YYYY-MM-DD HH:mm:ss')}>
          <span>{dayjs().format('YYYY-MM-DD HH:mm:ss')}</span>
        </Tooltip>
      }
    />
  )
};
