import React, { useImperativeHandle, useRef } from 'react';
import { CrudFilter, CrudOperators, useList } from '@pankod/refine';
import { definitions } from 'interfaces/supabase';
import { Comment, Typography, Row, Col, Button, Divider } from 'antd';
import { IdeaComment } from './IdeaComment';
import { FormComment } from './FormComment';

export type IdeaCommentsProps = {
  id: string;
  comment_id?: string;
  depth?: number;
};

export type IdeaCommentsRef = {
  commentsLength: () => number;
};

export const IdeaComments = React.forwardRef(
  (
    { id, comment_id, depth = 0 }: IdeaCommentsProps,
    ref: React.ForwardedRef<IdeaCommentsRef>,
  ) => {
    const ideaFilter: CrudFilter = { field: 'idea_id', value: id, operator: 'eq' };
    const [responsesVisible, setResponsesVisible] = React.useState<boolean>(depth !== 0 ? true : false);
    const [formVisible, setFormVisible] = React.useState<boolean>(false);
    const [ideaFormVisible, setIdeaFormVisible] = React.useState<boolean>(false);
    const { data } = useList<definitions['IdeaComments']>({
      resource: 'IdeaComments',
      config: {
        pagination: { pageSize: 10000 },
        filters: (comment_id
          ? [
            ideaFilter,
            { field: 'comment_id', operator: 'eq', value: comment_id }
          ]
          : [
            ideaFilter,
            { field: 'comment_id', operator: 'null', value: 'null' }
          ]
        )
      }
    });
    return (
      <Row style={{ marginLeft: depth * 48 }}>
        <Col span={12}>
          {depth === 0 && <Typography.Title level={3}>Комментарии</Typography.Title>}
        </Col>
        <Col span={12} style={{ textAlign: 'right' }}>
          {depth === 0 && (
            <Button
              type="link"
              onClick={() => {
                if (ideaFormVisible) {
                  setIdeaFormVisible(false);
                } else {
                  setIdeaFormVisible(true);
                }
              }}
            >
              {ideaFormVisible ? 'Отмена' : 'Комментировать'}
            </Button>
          )}
        </Col>
        {ideaFormVisible && (
          <Col span={24}>
            <FormComment
              afterSubmit={() => {
                setIdeaFormVisible(false);
              }}
              idea_id={id}
            />
          </Col>
        )}
        <Col span={24}>
          {
            data?.data.map((comment) => {
              if (comment.id) return (
                <>
                  <IdeaComment
                    key={comment.id}
                    comment={comment}
                  />
                  <Button
                    type="link"
                    onClick={() => {
                      if (formVisible) {
                        setFormVisible(false);
                      } else {
                        setFormVisible(true);
                      }
                    }}
                  >
                    {formVisible ? 'Отмена' : 'Ответить'}
                  </Button>
                  {
                    depth === 0 &&
                    <Button
                      type="link"
                      onClick={() => {
                        if (responsesVisible) {
                          setResponsesVisible(false);
                        } else {
                          setResponsesVisible(true);
                        }
                      }}
                    >
                      {responsesVisible ? 'Скрыть' : 'Развернуть'}
                    </Button>
                  }
                  <Divider />
                  {responsesVisible && <IdeaComments id={id} comment_id={comment.id} depth={depth + 1} />}
                  {formVisible && <FormComment
                    afterSubmit={() => {
                      setFormVisible(false);
                    }}
                    comment_id={comment.id}
                    idea_id={id}
                  />}
                </>
              );
              return <></>;
            })
          }
        </Col>
      </Row>
    );
  });
