/*
.richeditor {
  border: 1px solid #8a8886,
  box-sizing: border-box;
  display: flex;
  flex-direction: column;

  &-error {
    color: rgb(164, 38, 44);
    width: 100%;
  }

  >.DraftEditor-root {
    padding: 16px;
    overflow-y: auto;
    word-break: break-word;
    height: ~"calc(100% - 40px)";
    box-sizing: border-box;
    position: relative;
    flex: 1 0 auto;

    .public-DraftEditorPlaceholder-root {
      width: ~"calc(100% - 32px)";
      color: #D2D0ce;
      font-size: 16px;
      line-height: 22px;
    }

    .DraftEditor-editorContainer {
      position: absolute;
      height: ~"calc(100% - 32px)";
      width: ~"calc(100% - 32px)";

      .public-DraftEditor-content {
        height: 100%;
      }
    }
  }

  &>&-header {
    height: 40px;
    background: #ffffff;
    border-radius: 2px 2px 0px 0px;
    border-bottom: 1px solid #edebe9;
    color: #212121;
    display: flex;
    align-items: center;
    font-weight: normal;
    font-size: 18px;
    padding-left: 16px;
    padding-right: 16px;
    position: relative;
    border-bottom: 1px solid #edebe9;

    &>div {
      margin-right: 38px;
      display: flex;
    }

    .richeditor-editor-controls {
      margin-right: 0;
      position: absolute;
      right: 0;
      display: flex;
      top: 11px;
    }

    .control-icon {
      margin-right: 16px;
      cursor: pointer;
    }
  }

  &__decoration-controls,
  &__text-align-controls {
    display: flex;

    &>* {
      cursor: pointer;
    }
  }
}
*/

import { styleConfig } from "theme";

export const richeditor = styleConfig.css({
  border: '1px solid #8a8886',
  boxSizing: 'border-box',
  display: 'flex',
  flexDirection: 'column',
  minHeight: '250px',
  borderRadius: '2px',
  '& .DraftEditor-root': {
    padding: '16px',
    overflowY: 'auto',
    wordBreak: 'break-word',
    height: 'calc(100% - 40px)',
    boxSizing: 'border-box',
    position: 'relative',
    flex: '1 0 auto',

    '& .public-DraftEditorPlaceholder-root': {
      width: 'calc(100% - 32px)',
      color: '#D2D0ce',
      fontSize: '16px',
      lineHeight: '22px',
    },

    '& .DraftEditor-editorContainer': {
      position: 'absolute',
      height: 'calc(100% - 32px)',
      width: 'calc(100% - 32px)',
      '& .public-DraftEditor-content': {
        height: '100%',
      }
    }
  }
});

export const richeditorHeader = styleConfig.css({
  height: '40px',
  background: 'inherit',
  borderRadius: '2px 2px 0px 0px',
  borderBottom: '1px solid #edebe9',
  color: '#ebebeb',
  display: 'flex',
  alignItems: 'center',
  fontWeight: 'normal',
  fontSize: '18px',
  paddingLeft: '16px',
  paddingRight: '16px',
  position: 'relative',

  '&>div': {
    marginRight: '38px',
    display: 'flex',
  },

  '.richeditor-editor-controls': {
    marginRight: 0,
    position: 'absolute',
    right: 0,
    display: 'flex',
    top: '11px',
  },
  '& .control-icon': {
    marginRight: '16px',
    cursor: 'pointer',
  }
})