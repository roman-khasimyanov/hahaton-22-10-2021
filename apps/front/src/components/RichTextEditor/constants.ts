export enum InlineStyles {
  BOLD = 'BOLD',
  ITALIC = 'ITALIC',
  UNDERLINE = 'UNDERLINE',
  LEFT = 'LEFT',
  CENTER = 'CENTER',
}

export enum BlockStyles {
  UL = 'unordered-list-item',
  OL = 'ordered-list-item',
}

export const customStyleMap = {
  [InlineStyles.CENTER]: {
    textAlign: 'center' as const,
    display: 'block',
  },
};

export const markdownToDraftOptions = {
  blockStyles: {
    ins_open: InlineStyles.UNDERLINE,
    mark_open: InlineStyles.CENTER,
  },
  remarkableOptions: {
    enable: {
      inline: ['ins', 'mark'],
    },
  },
  preserveNewlines: true,
};
