/* eslint-disable @typescript-eslint/naming-convention */
import * as React from 'react';
import {
  Editor,
  EditorState,
  RichUtils,
  convertToRaw,
  convertFromRaw,
  SelectionState,
  RawDraftContentBlock,
} from 'draft-js';
import {
  draftToMarkdown,
  markdownToDraft,
} from 'markdown-draft-js';
import 'draft-js/dist/Draft.css';
import {
  ItalicOutlined,
  OrderedListOutlined,
  UnorderedListOutlined,
  ClearOutlined,
  SaveOutlined,
  UnderlineOutlined,
  AlignCenterOutlined,
  AlignLeftOutlined,
  DeleteOutlined,
  BoldOutlined,
} from '@ant-design/icons';
import {
  BlockStyles, InlineStyles, customStyleMap, markdownToDraftOptions,
} from './constants';
import { richeditor, richeditorHeader } from './index.styles';

export type RichTextEditorProps = {
  initialValue?: string;
  onChange?: (value: string) => void;
  onBlur?: () => void;
  saveButtonVisible?: boolean;
  placeholder?: string;
  errorMessage?: string;
};

const RichTextEditor = ({
  onChange,
  initialValue,
  onBlur,
  saveButtonVisible,
  placeholder,
  errorMessage,
}: RichTextEditorProps): JSX.Element => {
  const [editorState, setEditorState] = React.useState(
    () => EditorState.createEmpty(),
  );
  const [isValueInitialized, setIsValueInitialized] = React.useState(false);
  const editorRef = React.useRef<Editor>(null);

  React.useEffect(() => {
    if (initialValue && !isValueInitialized) {
      const markdownString = initialValue;
      const rawData = markdownToDraft(
        markdownString,
        markdownToDraftOptions,
      );
      const contentState = convertFromRaw(rawData);
      const newEditorState = EditorState.moveFocusToEnd(EditorState.createWithContent(contentState));
      setEditorState(newEditorState);
      setIsValueInitialized(true);
    }
  }, [initialValue]);

  const currentStyle = editorState.getCurrentInlineStyle();

  const extendedStyleItems = {
    [InlineStyles.UNDERLINE]: {
      open: () => '++',
      close: () => '++',
    },
    [InlineStyles.CENTER]: {
      open: () => '==',
      close: () => '==',
    },
  };

  const _onChange = (changedValue: EditorState) => {
    setEditorState(changedValue);
    const content = changedValue.getCurrentContent();
    const rawObject = convertToRaw(content);
    const markdownString = draftToMarkdown(rawObject, { styleItems: extendedStyleItems });
    if (onChange) {
      onChange(markdownString);
    }
  };

  const getFisrtNotEmptyLine = (
    blocks: RawDraftContentBlock[],
    startIndex: number,
    endIndex: number,
  ) => {
    for (let index = startIndex; index <= endIndex; index += 1) {
      const block = blocks[index];
      if (block.text === '') continue;
      return block.key;
    }
    return blocks[endIndex].key;
  };

  const toggleInlineStyle = (inlineStyle: InlineStyles) => {
    const anchorKey = editorState.getSelection().getAnchorKey();
    const focusKey = editorState.getSelection().getFocusKey();
    const content = editorState.getCurrentContent();
    const { blocks } = convertToRaw(content);
    const anchorKeyIndex = blocks.findIndex((block) => block.key === anchorKey);
    const focusKeyIndex = blocks.findIndex((block) => block.key === focusKey);
    const startIndex = Math.min(focusKeyIndex, anchorKeyIndex);
    const endIndex = Math.max(focusKeyIndex, anchorKeyIndex);

    if (anchorKey === focusKey && inlineStyle === InlineStyles.CENTER) {
      const newSelectionState = SelectionState.createEmpty(focusKey);
      const updatedSelection = newSelectionState.merge({
        focusOffset: blocks[focusKeyIndex].text.length,
      });
      const newEditorState = EditorState.forceSelection(editorState, updatedSelection);
      _onChange(RichUtils.toggleInlineStyle(newEditorState, inlineStyle));
      return;
    }
    if (blocks[startIndex].text !== '') {
      _onChange(RichUtils.toggleInlineStyle(editorState, inlineStyle));
      return;
    }

    const notEmptyLineKey = getFisrtNotEmptyLine(blocks, startIndex, endIndex);
    const newSelection = SelectionState.createEmpty(blocks[endIndex].key);
    const updatedSelection = newSelection.merge({
      anchorOffset: editorState.getSelection().getAnchorOffset(),
      focusOffset: editorState.getSelection().getFocusOffset(),
      isBackward: editorState.getSelection().getIsBackward(),
      focusKey: focusKeyIndex === endIndex ? blocks[endIndex].key : notEmptyLineKey,
      anchorKey: anchorKeyIndex === endIndex ? blocks[endIndex].key : notEmptyLineKey,
    });
    const newState = EditorState.forceSelection(editorState, updatedSelection);
    _onChange(RichUtils.toggleInlineStyle(newState, inlineStyle));
  };

  const toggleBlockStyle = (blockStyle: BlockStyles) => {
    _onChange(RichUtils.toggleBlockType(editorState, blockStyle));
  };

  const onBoldClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleInlineStyle(InlineStyles.BOLD);
  };
  const onItalicClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleInlineStyle(InlineStyles.ITALIC);
  };
  const onUnderlineClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleInlineStyle(InlineStyles.UNDERLINE);
  };

  const onCenterAlignClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleInlineStyle(InlineStyles.CENTER);
  };

  const onOrderedListClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleBlockStyle(BlockStyles.OL);
  };
  const onUnorderedListClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    toggleBlockStyle(BlockStyles.UL);
  };

  const onClearClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    e.preventDefault();
    _onChange(EditorState.createEmpty());
    setTimeout(() => {
      if (editorRef) {
        editorRef.current?.blur();
      }
    });
  };
  const activeInlineControlsColor = (style: InlineStyles) => {
    if (currentStyle.has(style)) {
      return '#0078d4';
    }
    if (style === InlineStyles.LEFT && !currentStyle.has(InlineStyles.CENTER)) {
      return '#0078d4';
    }
    return '#ebebeb';
  };

  const activeBlockControlsColor = (style: BlockStyles) => {
    const selection = editorState.getSelection();
    const blockType = editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType();
    if (blockType === style) {
      return '#0078d4';
    }
    return '#ebebeb';
  };
  const orderStyle = convertToRaw(editorState.getCurrentContent()).blocks[0].type;
  return (
    <>
      <div
        className={richeditor()}
        style={errorMessage ? {
          borderColor: 'rgb(164, 38, 44)',
        } : undefined}
      >
        <div className={richeditorHeader()}>
          <div style={{ display: 'flex' }}>
            <BoldOutlined
              className="control-icon"
              onMouseDown={onBoldClick}
              style={{ color: activeInlineControlsColor(InlineStyles.BOLD) }}
            />
            <ItalicOutlined
              className="control-icon"
              onMouseDown={onItalicClick}
              style={{ color: activeInlineControlsColor(InlineStyles.ITALIC) }}
            />
            <UnderlineOutlined
              className="control-icon"
              onMouseDown={onUnderlineClick}
              style={{ color: activeInlineControlsColor(InlineStyles.UNDERLINE) }}
            />
          </div>
          <div style={{ display: 'flex' }}>
            <AlignLeftOutlined
              className="control-icon"
              onMouseDown={onCenterAlignClick}
              style={{ color: activeInlineControlsColor(InlineStyles.LEFT) }}
            />
            <AlignCenterOutlined
              onMouseDown={onCenterAlignClick}
              style={{ color: activeInlineControlsColor(InlineStyles.CENTER) }}
            />
          </div>
          <div className="richeditor-list-controls">
            <OrderedListOutlined
              className="control-icon"
              onMouseDown={onOrderedListClick}
              style={{ color: activeBlockControlsColor(BlockStyles.OL) }}
            />
            <UnorderedListOutlined
              className="control-icon"
              onMouseDown={onUnorderedListClick}
              style={{ color: activeBlockControlsColor(BlockStyles.UL) }}
            />
          </div>
          <div className="richeditor-editor-controls">
            <DeleteOutlined
              className="control-icon"
              onMouseDown={onClearClick}
            />
            {
              saveButtonVisible
              && (
                <SaveOutlined onMouseDown={() => {
                  if (typeof onBlur === 'function') {
                    onBlur();
                  }
                }}
                />
              )
            }
          </div>
        </div>
        <Editor
          placeholder={orderStyle === 'unstyled' ? placeholder : undefined}
          editorState={editorState}
          onChange={_onChange}
          onBlur={onBlur}
          customStyleMap={customStyleMap}
          ref={editorRef}
        />
      </div>
      {
        errorMessage && (
          <div role="alert" className="richeditor-error">{errorMessage}</div>
        )
      }
    </>
  );
};

export default RichTextEditor;
