import React from "react";
import { Skeleton } from "@pankod/refine";
import { Avatar, Spin } from "antd";
import { useUserById } from "hooks/useUserById";

type UserByIdProps = {
  id: string;
};

export const UserById = ({ id }: UserByIdProps) => {
  const user = useUserById(id);

  if (user) {
    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        <Avatar src={user.avatar_url} />
        <span style={{ margin: "0px 8px" }}>{user.full_name}</span>
      </div>
    );
  } else {
    return <Spin />;
  }
};
