import { Link, Icon } from '@pankod/refine';
import { Avatar, Space } from 'antd';
import { useUser } from 'hooks/useUser';

export const User = () => {
	const currentUser = useUser();

	return (
		<div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
			<div style={{ display: 'flex', alignItems: 'center' }}>
				<Icon component={() => <img src="/currency.png" style={{ width: 40, height: 40 }} />} />
				<span id="user_currency">{currentUser?.currency || 0}</span>
				<Icon component={() => <img src="/crown.png" style={{ width: 40, height: 32, marginLeft: '10px' }} />} />
				<span id="user_carma">{currentUser?.carma || 0}</span>
			</div>
			<div>
				<Link to='/profile' style={{ color: "white" }}>
					<span style={{ marginRight: 12 }}>{currentUser?.full_name}</span>
					<Avatar src={currentUser?.avatar_url} />
				</Link>
			</div>
		</div>
	);
};
