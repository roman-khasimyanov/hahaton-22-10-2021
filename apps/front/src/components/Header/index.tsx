import {
	AntdLayout,
	Space,
	Avatar,
	Typography,
	useGetLocale,
	useGetIdentity,
} from "@pankod/refine";
import { useEffect } from "react";
import dayjs from "dayjs";
import "dayjs/locale/ru";
import "dayjs/locale/en";
import { User } from "components/User";

const { Text } = Typography;

export const Header: React.FC = () => {
	const locale = useGetLocale();
	const currentLocale = locale();

	useEffect(() => {
		dayjs.locale(currentLocale);
	}, [currentLocale]);

	return (
		<AntdLayout.Header
			style={{
				display: "flex",
				justifyContent: "flex-end",
				alignItems: "center",
				padding: "0px 24px",
				height: "64px",
				width: '100%',
				backgroundColor: "#000",
			}}
		>
			<User />
		</AntdLayout.Header>
	);
};
