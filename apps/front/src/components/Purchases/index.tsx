import React from 'react';
import { ImageField, NumberField, TextField, useList, useTable, Icon } from '@pankod/refine';
import { definitions } from 'interfaces/supabase';
import { useUser } from 'hooks/useUser';
import { Table } from 'antd';

export const Purchases = () => {
  const user = useUser();
  const { data } = useList<definitions['Purchases']>({
    resource: 'Purchases',
    config: {
      filters: [{
        field: 'user_id',
        operator: 'eq',
        value: user?.auth_user_id,
      }],
      pagination: { pageSize: 10000 }
    },
  });

  const { tableProps } = useTable<definitions['Goods']>({
    resource: 'Goods',
    permanentFilter: [{
      field: 'id',
      operator: 'in',
      value: data?.data.map((p) => p.good_id),
    }],
    initialPageSize: 10000,
  });

  return (
    <Table {...tableProps}>
      <Table.Column
        dataIndex="name"
        title="Название"
        render={(value) => <TextField value={value} />}
      />
      <Table.Column
        dataIndex="description"
        title="Описание"
        render={(value) => <TextField value={value} />}
      />
      <Table.Column
        dataIndex="poster"
        title="Товар"
        width={'35%'}
        render={(value) => <ImageField value={value} height={150} width={150} />}
      />
      <Table.Column
        dataIndex="price"
        title="Цена"
        render={(value) => (
          <>
            <Icon component={() => <img src="/currency.png" style={{ width: 32, height: 32, marginTop: '-7px' }} alt="currency" />} />
            <NumberField value={value} />
          </>
        )}
      />
    </Table>
  );
};
