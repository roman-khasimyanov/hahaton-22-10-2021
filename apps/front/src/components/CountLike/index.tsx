import React, { useState } from "react";
import { HeartFilled, HeartOutlined } from "@ant-design/icons";
import { Button } from "antd";

type CountLikeProps = {
  isLiked: boolean;
  count: number;
  onClick?: (value: boolean) => void;
};

const heartStyle = {
  color: "red",
};

export const CountLike = ({ isLiked, count, onClick }: CountLikeProps) => {
  const [like, setLike] = useState<boolean>(isLiked);
  const [counted, setCounted] = useState<number>(count);

  const onClickButton: React.MouseEventHandler<HTMLElement> = (event) => {
    setLike(!like);
    onClick?.(!like);
    if (like) {
      setCounted(counted - 1);
    } else {
      setCounted(counted + 1);
    }
  };

  return (
    <div>
      <Button
        onClick={onClickButton}
        type="link"
        icon={
          like ? (
            <HeartFilled style={heartStyle} />
          ) : (
            <HeartOutlined style={heartStyle} />
          )
        }
      ></Button>
      {counted}
    </div>
  );
};
