import React, { useState } from "react";
import {
  Menu,
  Grid,
  Icons,
  useMenu,
  AntdLayout,
  useNavigation,
  Button,
  Affix,
} from "@pankod/refine";
import { antLayoutSider, antLayoutSiderMobile } from "./styles";
import { authProvider } from "authProvider";
import { LogoutOutlined } from "@ant-design/icons";

export const Sider: React.FC = () => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const { menuItems, selectedKey } = useMenu();
  const breakpoint = Grid.useBreakpoint();
  const { push } = useNavigation();

  const isMobile = !breakpoint.lg;

  return (
    <Affix>
      <AntdLayout.Sider
        collapsible
        collapsedWidth={isMobile ? 0 : 80}
        collapsed={collapsed}
        breakpoint="lg"
        onCollapse={(collapsed: boolean): void => setCollapsed(collapsed)}
        style={isMobile ? antLayoutSiderMobile : antLayoutSider}
      >
        <img
          src="/logo.png"
          alt="mycab_super_logo"
          style={{
            width: !collapsed ? "200px" : "85px",
            transition: "width .1s, padding .1s",
            padding: !collapsed ? "10px" : "20px",
          }}
        />
        <Menu
          style={{
            display: "flex",
            flexDirection: "column",
            height: "100%",
            transform: "translate 1s ease",
          }}
          selectedKeys={[selectedKey]}
          mode="inline"
          onClick={({ key }) => {
            if (!breakpoint.lg) {
              setCollapsed(true);
            }

            push(key as string);
          }}
        >
          {menuItems.map(({ icon, label, route }) => {
            const isSelected = route === selectedKey;
            return (
              <Menu.Item
                style={{
                  fontWeight: isSelected ? "bold" : "normal",
                }}
                key={route}
                icon={icon}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    transition: ".5s ease",
                  }}
                >
                  {label}
                  {!collapsed && isSelected && <Icons.RightOutlined />}
                </div>
              </Menu.Item>
            );
          })}
        </Menu>
        <div
          style={{
            position: "fixed",
            display: "flex",
            justifyContent: "center",
            bottom: "60px",
            zIndex: 20,
            width: collapsed ? "79px" : "200px",
            transition: "width .1s",
          }}
        >
          <Button
            type="text"
            style={{
              padding: 0,
              background: "#262626",
              width: "100%",
              height: "48px",
            }}
            onClick={() => authProvider.logout({}).then(() => push("/"))}
          >
            {<Icons.LogoutOutlined />}
          </Button>
        </div>
      </AntdLayout.Sider>
    </Affix>
  );
};
