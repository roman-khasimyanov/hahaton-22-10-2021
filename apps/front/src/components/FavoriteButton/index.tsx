import { Button } from "antd";
import React, { useState } from "react";
import { StarOutlined, StarFilled } from "@ant-design/icons";

type FavoriteButtonProps = {
  favorite: boolean;
  onClick?: (value: boolean) => void;
};

const iconStyle = {
  color: "#fb8c00",
};

export const FavoriteButton = ({ favorite, onClick }: FavoriteButtonProps) => {
  const [checked, setChecked] = useState<boolean>(favorite);

  return (
    <Button
      style={{
        padding: 0,
        marginRight: "10px",
      }}
      onClick={() => {
        setChecked(!checked);
        onClick?.(!checked);
      }}
      type="link"
      icon={
        checked ? (
          <StarFilled style={iconStyle} />
        ) : (
          <StarOutlined style={iconStyle} />
        )
      }
    >
      <span style={{ color: checked ? "#fb8c00" : "lightgrey" }}>
        {checked ? "В избранном" : "Добавить в избранное"}
      </span>
    </Button>
  );
};
