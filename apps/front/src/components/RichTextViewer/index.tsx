import * as React from 'react';
import {
  Editor,
  EditorState,
  convertFromRaw,
} from 'draft-js';
import {
  markdownToDraft,
} from 'markdown-draft-js';
import 'draft-js/dist/Draft.css';
import { customStyleMap, markdownToDraftOptions } from '../RichTextEditor/constants';
import './index.css';

export type RichTextViewerProps = {
  initialValue?: string;
  title?: string;
};

const RichTextViewer = ({ initialValue, title }: RichTextViewerProps): JSX.Element => {
  const [editorState, setEditorState] = React.useState(
    () => EditorState.createEmpty(),
  );

  React.useEffect(() => {
    const markdownString = initialValue || '';
    const rawData = markdownToDraft(
      markdownString,
      markdownToDraftOptions,
    );
    const contentState = convertFromRaw(rawData);
    const newEditorState = EditorState.createWithContent(contentState);
    setEditorState(newEditorState);
  }, [initialValue]);

  return (
    <div className="rich-text-viewer">
      <Editor
        editorState={editorState}
        onChange={() => setEditorState(editorState)}
        customStyleMap={customStyleMap}
      />
    </div>
  );
};

export default RichTextViewer;
